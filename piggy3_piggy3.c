// piggy3
#include <curses.h>
#include <locale.h>

#define NUMWINS 7
#define RES_BUF_SIZE 80

WINDOW *w[NUMWINS];
WINDOW *sw[NUMWINS];

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pwd.h>
#include <getopt.h>
#include <string.h>

#include <sys/select.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <errno.h>

#define MAXHOSTNAME 32
#define BACKLOG     5
#define esc         27
//--------------------------------------------------

int head = 0;                                 // flag for head piggy
int tail = 0;                                 // flag for tail piggy

int ii, ij;
int cursor;
char response[RES_BUF_SIZE];
int WPOS[NUMWINS][4] = { {16, 66, 0, 0}, {16, 66, 0, 66}, {16, 66, 16, 0}, {16, 66, 16, 66}, {3, 132, 32, 0}, {5, 132, 35, 0}, {3, 132, 40, 0} };

int command_num = 0;
char in1[1000];
char in2[1000];
char in3[10000];
char in4[1000];
char in5[1000];
char in6[10000];
char in7[1000];
char in8[1000];
char in9[10000];
char in10[1000];

char input[1000];                             // data from stdin

int sc, sc2, scs, ss1, ss2, ssc;              // socket descriptors for client and server                                 
int i;                                        // general purpose integers

int middle = 0;                               // flag for middle piggy

int client = 0;
int server = 0;
int send_sock = 0;
int serverl, serverr, clientl, clientr = 0;   // only for middle

int lport_given = 0;                          // flag if port was given
int rport_given = 0;
int rlport_given = 0;                         // flag for right local port

int remoteport, remoteportl = 0;                                
int localport, localportl = 0;

// Flags
int sockflag = 0;
int f_persl, f_persr = 0;
int f_loopr, f_loopl = 0;
int f_outl, f_outr = 0;
int f_droppedl, f_droppedr = 0;
int f_dropr, f_dropl = 0;

// reset information
char* a_rraddr;
int b_rrport;
int c_llport;
int d_rlport;
int e_persl;
int g_persr;
int h_loopr;
int i_loopl;

struct sockaddr_in sa_c, sa_c2, sa_s1, sa_s2;
struct sockaddr_in saddr;
struct hostent *hpc, *hpc2, *hps, *hps2;
char buf_l[1000];                               // data for left
char buf_l2[1000];                              // data from left

char buf_r[1000];                               // data for right
char buf_r2[1000];                              // data from right

int keys = 0;
int outkeys = 0;
int i2;

char *myname;                                 // name of program
char *host;                                   // name of remote host
char *hostl;
char localhost[MAXHOSTNAME+1];                // location for name of local host

int i3, rc, on = 1;                           // used for select()
int close_conn;
int max_sd, new_sd;
  int desc_ready, end_server = 0;               // set to FALSE
fd_set master_set, working_set;

int c, ci;

FILE *file;                                   // file pointer
char *token;
char commands[300];
int change_token = 1;

FILE *file2;
char *token2;
char commands2[300];
char* space;


char input2[1000];                            // for use with long interactive commands
char *token3;


main(argc, argv)
int argc;
char *argv[];
{
  space = ' ';
  char *filename = (char *)malloc(30);          // name of file of args
  char *filename2 = (char *)malloc(30);         // for use with interactive read
  
  //------------------------------------------------
  
  myname = argv[0];

  if(argc < 2){
    printf("Error: invalid arguments.\n\r");
    exit(1);
  }

  //------------------------------------------------

  setlocale(LC_ALL, "");
  initscr();
  cbreak();
  noecho();
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
  clear();

  

  //------------------------------------------------

  int long_index = 0;
    switch(opt){
  
      //------------------------------------------------
      case 's':
      // read a script from a file
	
	if(!optarg){
	  strcpy(filename, "scriptin.txt");
	} else {
	  strncpy(filename, optarg, 30);
	}

	wmove(sw[0], 0, 0);
	wprintw(sw[0], "Text file commands:\n");
	
	if((file = fopen(filename, "r")) != NULL){
	  
	fgets(commands, 300, file);
	
	token = strtok(commands, " ");

	while(token){
	 
	  if(strcmp(token, "output") == 0){
	    if(head == 1 || f_outl == 0){
	      wprintw(sw[0], "output: Left to right\n");
	    } else {
	      wprintw(sw[0], "output: Right to left\n");
	    }
	    
	  } else if(strcmp(token, "outputl") == 0){
	    if(head == 1 || tail == 1){
	      wprintw(sw[0], "outputl: output direction can't be changed for this piggy type.\n");
	    } else {
	      f_outl = 1;
	      f_outr = 0;
	      wprintw(sw[0], "outputl: Output direction is to the left.\n");
	    }

	  } else if(strcmp(token, "dropr") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "dropr: a tail piggy has no right connection.\n");
	    } else {
	      f_dropr = 1;
	      if(client == 1 || clientr == 1){
		if(FD_ISSET(sc, &master_set)){
		  close(sc);
		  FD_CLR(sc, &master_set);
		}
	      } 
	      wprintw(sw[0], "dropr: dropping the right side connection.\n");
	    }
	    
	  } else if(strcmp(token, "dropl") == 0){
	    if(head == 1){
	      wprintw(sw[0], "dropl: a head piggy doesn't have a left connection.\n");
	    } else {
	      f_dropl = 1;
	      if(serverl == 1 || server == 1){
		if(FD_ISSET(ss1, &master_set)){
		  close(ss1);
		  FD_CLR(sc, &master_set);
		}
	      } else {
		if(FD_ISSET(ssc, &master_set)){
		  close(ssc);
		  FD_CLR(sc, &master_set);
		}
	      }
	      wprintw(sw[0], "dropl: dropping the left side connection.\n");
	    }

	  } else if(strcmp(token, "persl") == 0){
	    if(head == 1){
	      wprintw(sw[0], "persl: a head piggy doesn't have a left connection.\n");
	    } else {
	      wprintw(sw[0], "persl: Left connection is persistent.\n");
	    }

	  } else if(strcmp(token, "persr") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "persr: a tail piggy can't have a right connection.\n");
	    } else {
	      wprintw(sw[0], "persr: Right connection is persistent.\n");
	    }

	  }else if(strcmp(token, "right") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "right: a tail piggy can't have a right connection.\n");
	    } else{
	      if(remoteport == 0 || f_droppedr == 1 || f_dropr == 1 || close_conn == 1){
	        wprintw(sw[0], "Right connection: %s:%d:*:*\tNONE", localhost, localport);
	      } else{
	        wprintw(sw[0], "Right connection: %s:%d:%s:%d\t", localhost, localport, host, remoteport);
		if(remoteport != 0){
		  wprintw(sw[0], "CONNECTED\n");
		} else {
		  wprintw(sw[0], "LISTENING\n");
		}
	      }
	    }

	  } else if(strcmp(token, "left") == 0){
	    if(head == 1){
	      wprintw(sw[0], "left: a head piggy doesn't have a left connection.\n\r");
	    }else {
	      if(remoteport == 0 || f_droppedl == 1 || f_dropl == 1){
	        wprintw(sw[0], "Left connection: *:*:%s:%d\tNONE", localhost, localport);
	      } else if(tail == 1){
	        wprintw(sw[0], "Left connection: %s:%d:%s:%d\t", host, remoteport, localhost, localport);
		if(remoteport != 0){
		  wprintw(sw[0], "CONNECTED\n");
		} else {
		  wprintw(sw[0], "LISTENING\n");
		}
	      } else { // the middle piggy uses different variables
	        wprintw(sw[0], "Left connection: %s:%d:%s:%d\t", hostl, remoteportl, localhost, localportl);
		if(remoteport != 0){
		  wprintw(sw[0], "CONNECTED\n");
		} else {
		  wprintw(sw[0], "LISTENING\n");
		}
	      }
	    }
	    
	  } else if(strcmp(token, "loopr") == 0){
	    if(head == 1){
	      wprintw(sw[0], "loopr: a head piggy won't have any use for this.\n");
	    } else {
	      f_loopr = 1;
	      wprintw(sw[0], "loopr: Rightward data will go left.\n");
	    }
	    
	  } else if(strcmp(token, "loopl") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "loopl: a tail piggy won't have any use for this.\n");
	    } else {
	      f_loopl = 1;
	      wprintw(sw[0], "loopl: Leftward data will go right.\n");
	    }

	  } else if(strcmp(token, "connectr") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "connectr: a tail piggy can't use this.");
	    } else {
	      host = strtok(NULL, " ");
	      if(client == 1 || clientr == 1){
		if(FD_ISSET(sc, &master_set)){
		  close(sc);
		  FD_CLR(sc, &master_set);
		}
	      } else {
		if(FD_ISSET(scs, &master_set)){
		  close(scs);
		  FD_CLR(scs, &master_set);
		}
		if(FD_ISSET(sc2, &master_set)){
		  close(sc2);
		  FD_CLR(sc2, &master_set);
		}
	      }
	      if(middle == 1){
		clientl = 0;
		serverr = 0;
		clientr = 1;
		serverl = 1;
	      } else {
		server = 0;
		client = 1;
	      }
	      
	      // Look up given hostname
	      if((hpc = gethostbyname(host)) == NULL){
	        wprintw(sw[0], "%s: no such host?\n", host);
	      } else {
		// Put host's address and type into socket struct
		bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
		sa_c.sin_family = hpc->h_addrtype;
		
		if(atoi((token = strtok(NULL, " "))) >= 1 && atoi(token) < 65535){ // this is a port
		  sa_c.sin_port = atoi(token);
		  rport_given = 1;
		} else { // this is not a port
		  change_token = 0; // don't change the token at the end of the while loop
		  if(rport_given == 0){
		    sa_c.sin_port = 36762;
		  }
		}

		// allocate open socket
		if((sc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		  wmove(sw[6], 0, 0);
		  update_win(6);
		  perror("socket");
		}
		
		connect_sock(sc, &sa_c, sizeof sa_c);
		f_dropr = 0;
		f_droppedr = 0;
		remoteport = sa_c.sin_port;
	      }
	    }
	  
	  } else if(strcmp(token, "connectl") == 0){
	    if(head == 1){
	      wprintw(sw[0], "connectl: a head piggy can't use this.");
	    } else {
	      if(client == 1 || clientl == 1){
		if(FD_ISSET(ssc, &master_set)){
		  close(ssc);
		  FD_CLR(ssc, &master_set);
		}
	      }
	      
	      hostl = strtok(NULL, " ");

	      
	      
	      if(atoi((token = strtok(NULL, " "))) >= 1 && atoi(token) < 65535){ // this is a port
		sa_s1.sin_port = atoi(token);
		lport_given = 1;
	      } else { // this is not a port
		change_token = 0; // don't change the token at the end of the while loop
		if(lport_given == 0){
		  sa_s1.sin_port = 36762;
		}
	      }

	      
	      // allocate socket for connectl
	      if((ssc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		wmove(sw[6], 0, 0);
		update_win(6);
		perror("socket");
	      }
	      connect_sock(ssc, &sa_s1, sizeof sa_s1);
	      f_dropl = 0;
	      f_droppedl = 0;
	      remoteportl = sa_c.sin_port;
	    }
	    
	  } else if(strcmp(token, "listenl") == 0){
	    if(head == 1){
	      wprintw(sw[0], "listenl: a head piggy can't use this.");
	    } else {
	      if(clientl == 1){
		if(FD_ISSET(ssc, &master_set)){
		  close(ssc);
		  FD_CLR(ssc, &master_set);
		}
	      } else {
		if(FD_ISSET(ss1, &master_set)){
		  close(ss1);
		  FD_CLR(ss1, &master_set);
		}
		if(FD_ISSET(ss2, &master_set)){
		  close(ss2);
		  FD_CLR(ss2, &master_set);
		}
	      }
	      if(middle == 1){
		clientl = 0;
		serverr = 0;
		clientr = 1;
		serverl = 1;
	      } else {
		server = 1;
		client = 0;
	      }
	      if(atoi((token = strtok(NULL, " "))) >= 1 && atoi(token) < 65535){ // this is a port
		sa_s1.sin_port = atoi(token);
		lport_given = 1;
	      } else { // this is not a port
		change_token = 0; // don't change the token at the end of the while loop
		if(sa_s1.sin_port == 0){
		  sa_s1.sin_port = 36762;
		}
	      }

	      // allocate open socket
	      if((ss1 = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		wmove(sw[6], 0, 0);
		update_win(6);
		perror("socket");
	      }

	      bind_listen(ss1, &sa_s1, sizeof sa_s1);
	      if(middle == 1){
		localportl = sa_s1.sin_port;
	      } else {
		localport = sa_s1.sin_port;
	      }
	      
	      // Add new connection to the master set
	      FD_SET(ss1, &master_set);
	      if(ss1 > max_sd){
		max_sd = ss1;
	      }

	    }
	    
	  } else if(strcmp(token, "listenr") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "listenr: a tail piggy can't use this.");
	    } else {
	      if(clientr == 1){
		if(FD_ISSET(sc, &master_set)){
		  close(sc);
		  FD_CLR(sc, &master_set);
		}
	      } else {
		if(FD_ISSET(scs, &master_set)){
		  close(scs);
		  FD_CLR(scs, &master_set);
		}
		if(FD_ISSET(sc2, &master_set)){
		  close(sc2);
		  FD_CLR(sc2, &master_set);
		}
	      }	      if(middle == 1){
		clientr = 0;
		serverl = 0;
		clientl = 1;
		serverr = 1;
	      } else {
		client = 0;
		server = 1;
	      }
	      
	      if(atoi((token = strtok(NULL, " "))) >= 1 && atoi(token) < 65535){ // this is a port
		sa_c.sin_port = atoi(token);
		rport_given = 1;
	      } else { // this is not a port
		change_token = 0; // don't change the token at the end of the while loop
		if(sa_c.sin_port == 0){
		  sa_c.sin_port = 36762;
		}
	      }

	      // allocate open socket
	      if((scs = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		wmove(sw[6], 0, 0);
		update_win(6);
		perror("socket");
	      }

	      bind_listen(scs, &sa_c, sizeof sa_c);
	      if(scs > max_sd){
		max_sd = scs;
	      }
	      
	      // Add new connection to the master set
	      FD_SET(scs, &master_set);
	      if(scs > max_sd){
		max_sd = scs;
	      }

	    }

	  } else if(strcmp(token, "read") == 0){
	    strncpy(filename2, strtok(NULL, " "), 30);

	    if((file2 = fopen(filename2, "r")) != NULL){
	      fgets(commands2, 300, file2);
	      token2 = strtok(commands2, " ");

	      if(head == 1 || f_outl == 0){
		if(client == 1 || clientr == 1){
		  send_sock = sc;
		} else{
		  send_sock = sc2;
		}
	      } else {
		if(server == 1 || serverl == 1){
		  send_sock = ss2;
		} else {
		  send_sock = ssc;
		}
	      }
	      
	      while(token2){
		send(send_sock, token2, sizeof(token2), 0);
		send(send_sock, space, sizeof(space), 0);
		
		if(head == 1 || f_outl == 0){ // send to right
		  wprintw(sw[1], token2);
		  wprintw(sw[1], " ");
		  update_win(1);
		} else {
		  wprintw(sw[2], token2);
		  wprintw(sw[2], " ");
		  update_win(2);
		}
		token2 = strtok(NULL, " ");
	      }
	    } else {
	      wprintw(sw[0], "read: Invalid file to read.\n");
	    }


	  } else if(strcmp(token, "llport") == 0){
	    if(head == 1){
	      wprintw(sw[0], "llport: a head piggy can't use this.\n");
	    } else {
	      token = strtok(NULL, " ");

	      if(atoi(token) >= 1 && atoi(token) < 65535){ // this is a valid port
		localportl = atoi(token);
		if(tail == 1){
		  localport = atoi(token);
		}
		if(clientl == 1 || client == 1){
		  saddr.sin_port = atoi(token);

		  //Bind socket to service port
		  if(bind(ssc, &saddr, sizeof saddr) < 0){
		    perror("bind");
		  }
		} else {
		  sa_s1.sin_port = atoi(token);

		  //Bind socket to service port
		  if(bind(ss1, &saddr, sizeof saddr) < 0){
		    perror("bind");
		  }
		}
	      } else { // not a valid port
		wprintw(sw[4], "Error: invalid port number.");
	      }
	    
	    }
	  } else if(strcmp(token, "rlport") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "rlport: a tail piggy can't use this.\n");
	    } else{
	      token = strtok(NULL, " ");
	      if(atoi(token) >= 1 && atoi(token) < 65535){ // this is a valid port
		localport = atoi(token);
		if(clientr == 1){
		  saddr.sin_port = atoi(token);

		  //Bind socket to service port
		  if(bind(sc, &saddr, sizeof saddr) < 0){
		    perror("bind");
		  }
		} else {
		  sa_s1.sin_port = atoi(token);

		  //Bind socket to service port
		  if(bind(scs, &saddr, sizeof saddr) < 0){
		    perror("bind");
		  }
		}
	      } else {
		wprintw(sw[4], "Error: invalid port number.");
	      }
	    }

	  } else if(strcmp(token, "lrport") == 0){
	    if(head == 1){
	      wprintw(sw[0], "lrport: a head piggy can't use this.\n");
	    } else {
	      token = strtok(NULL, " ");
	      if(atoi(token) >= 1 && atoi(token) < 65535){ // this is a valid port
		if(clientl == 1 || client == 1){
		  sa_c.sin_port = atoi(token);
		  remoteportl = atoi(token);
		} else{
		  do{
		    // Accept each incoming connection
		    i3 = sizeof sa_s2;
		    if((ss2 = accept(ss1, &sa_s2, &i3)) < 0){
		      if(errno != EWOULDBLOCK){
			wmove(sw[6], 0, 0);
			update_win(6);
			perror("accept");
		      }
		    }
		    f_dropl = 0;
		    f_droppedl = 0;

		    remoteportl = sa_s2.sin_port;
		    hps2 = gethostbyaddr(&sa_s2.sin_addr, sizeof(sa_s2.sin_addr), AF_INET);
		    hostl = hpc2->h_name;

		    if(atoi(token) != sa_s2.sin_port){
		      f_dropl = 1;
		    } else {
		      FD_SET(ss2, &master_set);
		      if(ss2 > max_sd){
			max_sd = ss2;
		      }
		    }

		  }while(f_dropl == 1 || f_droppedl == 1);

		}
	      } else {
		wprintw(sw[4], "Error: invalid port number.");
	      }
	    }
	    
	  } else if(strcmp(token, "lraddr") == 0){
	    if(head == 1){
	      wprintw(sw[0], "lraddr: a head piggy can't use this.\n");
	    } else {
	      token = strtok(NULL, " ");

	      // Look up given hostname
	      if((hps2 = gethostbyname(token3)) == NULL){
		wprintw(sw[6], "%s: no such host?", token);
	      } else {
		if(clientl == 1 || client == 1){
		  // Put host's address and type into socket struct
		  bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
		  sa_c.sin_family = hpc->h_addrtype;
		} else {
		  do{
		    // Accept each incoming connection
		    i3 = sizeof sa_s2;
		    if((ss2 = accept(ss1, &sa_s2, &i3)) < 0){
		      if(errno != EWOULDBLOCK){
			wmove(sw[6], 0, 0);
			update_win(6);
			perror("accept");
		      }
		    }
		    f_dropl = 0;
		    f_droppedl = 0;

		    remoteportl = sa_s2.sin_port;
		    hpc2 = gethostbyaddr(&sa_s2.sin_addr, sizeof(sa_s2.sin_addr), AF_INET);
		    hostl = hpc2->h_name;

		    if(strcmp(token, hostl) != 0){
		      f_dropl = 1;
		    } else {
		      FD_SET(ss2, &master_set);
		      if(ss2 > max_sd){
			max_sd = ss2;
		      }
		    }

		  }while(f_dropl == 1 || f_droppedl == 1);

		}
	      }
	    }
	  } else if(strcmp(token, "rraddr") == 0){
	    if(tail == 1){
	      wprintw(sw[0], "rraddr: a tail piggy can't use this.\n");
	    } else {
	      token = strtok(NULL, " ");
	      // Look up given hostname
	      if((hpc = gethostbyname(token)) == NULL){
		wprintw(sw[6], "%s: no such host?", token);
	      } else {
		if(clientr == 1){
		  // Put host's address and type into socket struct
		  bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
		  sa_c.sin_family = hpc->h_addrtype;
		} else {
		  do{
		    // Accept each incoming connection
		    i3 = sizeof sa_s2;
		    if((sc2 = accept(scs, &sa_c2, &i3)) < 0){
		      if(errno != EWOULDBLOCK){
			wmove(sw[6], 0, 0);
			update_win(6);
			perror("accept");
		      }
		    }
		    f_dropr = 0;
		    f_droppedr = 0;

		    remoteport = sa_c2.sin_port;
		    hpc2 = gethostbyaddr(&sa_c2.sin_addr, sizeof(sa_c2.sin_addr), AF_INET);
		    host = hpc2->h_name;

		    if(strcmp(token, host) != 0){
		      f_dropr = 1;
		    } else {
		      FD_SET(sc2, &master_set);
		      if(sc2 > max_sd){
			max_sd = sc2;
		      }
		    }

		  }while(f_dropr == 1 || f_droppedr == 1);

		}
	      }
	    }
	  } else if(strcmp(token, "reset") == 0){
	    reset();
	    
	  } else if(strcmp(token, "q") == 0){
	    nocbreak();
	    endwin();
	    exit(0);

	  } else if(strcmp(token, "\n") == 0){
	    wprintw(sw[4], "\n\r");
	    
	  } else {
	    wprintw(sw[4], "%s: Unknown command", token);
	  }
	  if(change_token == 1){ // only change the token if flag wasn't altered
	    token = strtok(NULL, " ");
	  } else{
	    change_token = 1; // reset the flag
	  }
	  update_all();
      	} // end while
	
      	fclose(file);
	}
       break;
      //------------------------------------------------
    case 'h':
      // head/noleft; acting as client

      head = 1;
      if(tail == 1){
	printf("Error: can't choose noleft and noright.\n\r");
	exit(1);
      }

      client = 1;
      
      // Get our own host info
      gethostname(localhost, MAXHOSTNAME);

      break;
      //------------------------------------------------
    case 't':
      // tail/noright; acting as server

      tail = 1;
      if(head == 1){
	printf("Error: can't choose both noright and noleft.\n\r");
	exit(1);
      }
      if(middle == 1){
	printf("Error: can't be both middle and noright\n\r");
	exit(1);
      }

      server = 1;

      // Get our own host info
      gethostname(localhost, MAXHOSTNAME);
      if((hps = gethostbyname(localhost)) == NULL){
	fprintf(stderr, "%s: cannot get local host info?\n", myname);
	exit(1);
      }

      // Put socket number and our addr info into socket struct
      sa_s1.sin_port = 36762;
      bcopy((char *)hps->h_addr, (char *)&sa_s1.sin_addr, hps->h_length);
      sa_s1.sin_family = hps->h_addrtype;
      localport = sa_s1.sin_port;
    
      break;
      //------------------------------------------------
    case 'a':
      // rraddr; acting as a client

      // Can't be a tail piggy
      if(tail == 1){
	printf("Error: tail piggy can't use rraddr.\n\r");
	exit(1);
      }

      if(head == 0){
	middle = 1;
      }

      host = optarg;
      a_rraddr = host;

      // Look up given hostname
      if((hpc = gethostbyname(host)) == NULL){
	fprintf(stderr, "%s: %s: no such host?\n", myname, host);
	exit(1);
      }

      // Put host's address and type into socket struct
      bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
      sa_c.sin_family = hpc->h_addrtype;
      
      break;
      
      //------------------------------------------------
    case 'r':
      // rrport; assigns client port

      if(tail == 1){
	printf("Error: tail piggy can't use rrport.\n\r");
	exit(1);
      }

      sa_c.sin_port = atoi(optarg);
      rport_given = 1;

      if(sa_c.sin_port < 1 || sa_c.sin_port > 65535){
	wprintw(sw[6], "Error: invalid port number.");
	update_win(6);
      }
      b_rrport = atoi(optarg);
    
      
      break;
      //------------------------------------------------
    case 'l':
      // llport; assigns server port

      if(head == 1){
	printf("Error: head piggy can't use llport.\n\r");
	exit(1);
      }
     
      sa_s1.sin_port = atoi(optarg);
      lport_given = 1;

      if(sa_s1.sin_port < 1 || sa_s1.sin_port > 65535){
	wprintw(sw[6], "Error: invalid port number.");
        update_win(6);
      }
      c_llport = atoi(optarg);
      

      break;
      //------------------------------------------------
    case 'u':
      // rlport; source port for right connection

      if(tail == 1){
	printf("Error: tail piggy can't use rlport.\n\r");
      } else {
	if(client == 1 || clientr == 1){
	  send_sock = sc;
	}else {
	  send_sock = sc2;
	}
        
	if(atoi(optarg) >= 1 && atoi(optarg) < 65535){ // this is a valid port
	  saddr.sin_port = atoi(optarg);
	  localport = atoi(optarg);
	  d_rlport = atoi(optarg);
	  //Bind socket to service port
	  if(bind(send_sock, &saddr, sizeof saddr) < 0){		
	    perror("bind");
	  }
	} else {
	  wprintw(sw[4], "Error: invalid port number.");
	}	
      }

      rlport_given = 1;

      //------------------------------------------------
    case 'm':
      // persl; make left side a "persistent connection"

      e_persl = 1;
      break;
      //------------------------------------------------
    case 'n':
      // persr; make right side a "persistent connection"
      
      g_persr = 1;

      break;
      //------------------------------------------------
    case 'e':
      // loopr; take data arriving from left and inject it into data leaving the left

      f_loopr = 1;
      h_loopr = 1;

      break;
      //------------------------------------------------
    case 'f':
      // loopl; take data coming in from right and inject it into data leaving the right

      f_loopl = 1;
      i_loopl = 1;

      break;
      //------------------------------------------------
    
    default: printf("Invalid parameters.\n\r");
      exit(1);
    }
  }

  //-----------------------------------------------------------------------------------------------
  
  if(tail == 0 && head == 0 && middle == 0){
    printf("Error: need a left or right address.\n\r");
    exit(1);
  }

  make_wins();
  run_piggy();
  
} // end main




  //-----------------------------------------------------------------------------------------------
void run_piggy(){

  char *filename2 = (char *)malloc(30);         // for use with interactive read

  //-----------------------------------------------------------------------------------------------
  
  if(head == 1){ // head piggy	  
    
    // Place cursor at top corner of window 5
    wmove(sw[4], 0, 0);
    wclrtoeol(sw[4]);
    waddstr(sw[4], "Commands");
    update_win(4);      
    

    // Create client socket
    if((sc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
      wmove(sw[6], 0, 0);
      update_win(6);
      perror("socket");
    }

    if(rport_given == 0){
      sa_c.sin_port = 36762;
    }

    remoteport = sa_c.sin_port;
    
    // Connect to remote server
    connect_sock(sc, &sa_c, sizeof sa_c);

    i3 = sizeof(saddr);
    getsockname(sc, (struct sockaddr *)&saddr, (socklen_t *)&i3);
    localport = (int)ntohs(saddr.sin_port);
    
    do{
      if(f_droppedr == 1 && f_persr == 1){
	// Reconnect to remote server
	connect_sock(sc, &sa_c, sizeof sa_c);
	
	i3 = sizeof(saddr);
	getsockname(sc, (struct sockaddr *)&saddr, (socklen_t *)&i3);
	localport = (int)ntohs(saddr.sin_port);
	
      } // end if(f_dropped...)
      
      // Initiate master fd_set
      FD_ZERO(&master_set);
      max_sd = sc;
      FD_SET(sc, &master_set);
      FD_SET(0, &master_set);     // watch stdin for input


    
      // Call select
      if((rc = select(max_sd + 1, &working_set, NULL, NULL, NULL)) <= 0){
	wmove(sw[6], 0, 0);
	update_win(6);
	perror("select");
	break;
      }
      
      // Determine which descriptors are readable
      desc_ready = rc;

      for(i = 0; i <= max_sd && desc_ready > 0; ++i){
	// Check if descriptor is ready
	if(FD_ISSET(i, &working_set)){
	  desc_ready -= 1;
	  
	  if(i == 0){ // stdin is readable
	    close_conn = 0;
	    wmove(sw[4], 0, 0);
	    wclrtoeol(sw[4]);
	    waddstr(sw[4], "Command Mode:");
	    update_win(4);
	    
	    keys = 0; // index for stdin buffer	    
	    cursor = 0;
	    
	    while(1){
	      switch(ci){
		
	      case 113: // q
		// Quitting
		nocbreak();

		
		endwin();
		exit(0);
		
	      case 105: // i
		// Insert Mode
		wmove(sw[5], 0, 0);
		wclrtoeol(sw[5]);
		wprintw(sw[5], "Insert Mode\n");
		update_win(5);
		
		outkeys = 0; // index for outgoing buffer
		
		while(c != esc){
		  
		  switch(c){
		  case esc: // ESC
		    outkeys = 0;
		    buf_r[0] = '\0';

		    // Back to Command Mode
		    wmove(sw[4], 0, 0);
		    wprintw(sw[4], "Command Mode: ");
		    update_win(4);
		    
		    break;

		  case 13: // RET
		    
		    buf_r[outkeys] = '\0';

		    if(client == 1){
		      send_sock = sc;
		    } else {
		      send_sock = sc2;
		    }
		    //send buffer to outgoing socket
		    if(send(send_sock, &buf_r, sizeof(buf_r), 0) != sizeof(buf_r)){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("send error");
		      break;
		    } else {
		      waddstr(sw[1], "\n");
		      waddstr(sw[1], &buf_r);
		      update_win(1);
		    }
		   
		    outkeys = 0;
		    buf_r[0] = '\0';
		    break;
		    
		  case KEY_BACKSPACE: // Backspace
		  case KEY_DC:
		  case 127:
      		    outkeys--;
		    if(outkeys < 0){
		      outkeys = 0;
		    }
		    
		    buf_r[outkeys] = '\0';
		    wclear(sw[5]);
		    
		    for(i2 = 0; i2 <= outkeys; i2++){
		      wprintw(sw[5], "%c", buf_r[i2]);
		    }
		    update_win(5);
		    
		    break;

		  default:
		    buf_r[outkeys] = c;
		    outkeys++;
		    wclear(sw[5]);
		    update_win(5);
		    
		    for(i2 = 0; i2 < outkeys; i2++){
		      wprintw(sw[5], "%c", buf_r[i2]);
		    }
		    
		  } // end switch(c)
		} // ESC was pressed, end insert mode
		
		default:
		  wclear(sw[4]);
		  if(ci != 105){ // lose 'i' from previous insert mode
		    input[cursor] = ci;
		    wprintw(sw[4], "%c", ci);
		    keys++;
		    cursor++;
		    wmove(sw[4], 0, cursor);
		    update_win(4);
		  }
		  
		  while(ci != 13){ // Return
		    
		    switch(ci){
		      
		      // Backspace
		    case KEY_BACKSPACE:
		    case KEY_DC:
		    case 127:
		      
		      keys--;
		      if(keys < 0){
			keys = 0;
		      }
		      cursor--;
		      if(cursor < 0){
			cursor = 0;
		      }

		      wclear(sw[4]);
		      
		      if(cursor == keys){
			input[cursor] = '\0';
			
			for(i2 = 0; i2 <= cursor; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
			
		      } else {
			// reset the buffer
			for(i2 = cursor; i2 < keys; i2++){
			  input[i2] = input[i2 + 1];
			}
			input[keys + 1] = '\0';
			input[keys] = '\0';
			
			for(i2 = 0; i2 <= keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
		        wmove(sw[4], 0, cursor);
			update_win(4);
		      }
		      break;
		      
		    case esc:
		      switch(sw[4]){

		      case 'A': // Up arrow
			wclear(sw[4]);
			command_num++;
			if(command_num > 10){
			  command_num = 1;
			}
			command_nums();
			wprintw(sw[4], "%s", &input);
			update_win(4);
			break;

		      case 'B': // Down arrow
			wclear(sw[4]);
			command_num--;
			if(command_num < 1){
			  command_num = 10;
			}
			command_nums();
			wprintw(sw[4], "%s", &input);
			update_win(4);
			break;
			
		      case 'D': // Left arrow
			wclear(sw[4]);
			cursor--;
			if(cursor < 0){
			  cursor = 0;
			}

			for(i2 = 0; i2 < keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
		        wmove(sw[4], 0, cursor);
			update_win(4);
			break;
			
		      case 'C': // right arrow
			wclear(sw[4]);
			cursor++;
			if(cursor > keys){
			  cursor = keys;
			}
			for(i2 = 0; i2 < keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
		        wmove(sw[4], 0, cursor);
			update_win(4);
			break;

		      case 'F': // END KEY
			cursor = keys;
			wmove(sw[4], 0, cursor);
			update_win(4);
			break;
			
		      } // end esc switch
		      break;
		      

		    default:
		      wclear(sw[4]);

		      if(cursor != keys) {
			for(i2 = keys; i2 > cursor; i2--){
			  input[i2] = input[i2 - 1];
			}
		      }
		      
		      input[cursor] = ci;
		      keys++;
		      cursor++;
		      for(i2 = 0; i2 < keys; i2++){
			wprintw(sw[4], "%c", input[i2]);
		      }
		      wmove(sw[4], 0, cursor);
		      update_win(4);

		    } // end switch
		  } // Return was pressed
		  
		  wclear(sw[4]);
		  if(command_num == 0){
		    input[keys] = '\0'; // Null terminate
		  }
		  command_num = 0;
		  
		  if(strcmp(input, "output\r") == 0){
		    wprintw(sw[4], "output: Left to right");
		    update_win(4);
		    
		  } else if(strcmp(input, "outputl\r") == 0){
		    wprintw(sw[4], "outputl: output direction can't be changed for this piggy type.");
		    update_win(4);
		      
		  } else if(strcmp(input, "outputr\r") == 0){
		    wprintw(sw[4], "outputr: output direction can't be changed for this piggy type.");
		    update_win(4);
		    
		  } else if(strcmp(input, "dropr\r") == 0){
		    f_dropr = 1;
		    close_conn = 1;
		    if(client == 1){
		      close(sc);
		      FD_CLR(sc, &master_set);
		    } else {
		      close(scs);
		      FD_CLR(scs, &master_set);
		      close(sc2);
		      FD_CLR(sc2, &master_set);
		    }
		    wprintw(sw[4], "dropr: dropping the right side connection.");
		    update_win(4);
		      
		  } else if(strcmp(input, "dropl\r") == 0){
		    wprintw(sw[4], "dropl: a head piggy doesn't have a left connection.");
		    update_win(4);
		    
		  } else if(strcmp(input, "persl\r") == 0){
		    wprintw(sw[4], "persl: a head piggy doesn't have a left connection.");
		    update_win(4);
		    
		  } else if(strcmp(input, "persr\r") == 0){
		    wprintw(sw[4], "persr: Right connection is persistent.");
		    update_win(4);
		    
		  } else if(strcmp(input, "right\r") == 0){
		    if(remoteport == 0 || close_conn == 1 || f_droppedr == 1){
		      wprintw(sw[4], "Right connection: %s:%d:*:*\tNONE", localhost, localport);
		    } else{
		      wprintw(sw[4], "Right connection: %s:%d:%s:%d\t", localhost, localport, host, remoteport);
		      if(remoteport != 0){
			wprintw(sw[4], "CONNECTED");
		      } else {
			wprintw(sw[4], "LISTENING");
		      }
		    }
		    update_win(4);
		    
		  } else if(strcmp(input, "left\r") == 0){
		    wprintw(sw[4], "left: a head piggy doesn't have a left connection.");
		    update_win(4);
		    
		  } else if(strcmp(input, "loopr\r") == 0){
		    wprintw(sw[4], "loopr: a head piggy won't have any use for this.");
		    update_win(4);
		    
		  } else if(strcmp(input, "loopl\r") == 0){
		    f_loopl = 1;
		    wprintw(sw[4], "loopl: Leftward data will go right.");
		    update_win(4);

		  } else if(strcmp(input, "listenl\r") == 0){
		    wprintw(sw[4], "Error: a head piggy can't use listenl.");
		    
		  } else if(strcmp(input, "listenr\r") == 0){
		    
		    client = 0;
		    server = 1;
                    
                    // allocate open socket
		    if((scs = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("socket");
		    }
		    
		    bind_listen(scs, &sa_c, sizeof sa_c);
		    if(scs > max_sd){
		      max_sd = scs;
		    }
		    FD_SET(scs, &master_set);
		  
		  } else if(strcmp(input, "reset\r") == 0){
		    reset();

		  } else if((keys - 1) > 7){ // a long command (with parameters) was called

		    strncpy(&input2, &input, 1000);
		    token3 = strtok(input2, " ");
		    
		    if(strcmp(token3, "connectr") == 0){
		      for(i2 = 0; i2 <= max_sd; ++i2){
			if(FD_ISSET(i2, &master_set)){
			  close(i2);
			}
		      }

		      client = 1;
		      server = 0;
		      
		      token3 = strtok(NULL, " ");
		      host = token3;

		      // Look up given hostname
		      if((hpc = gethostbyname(host)) == NULL){
		        wprintw(sw[6], "%s: no such host?\n", host);
		        break;
		      }

		      // Put host's address and type into socket struct
		      bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
		      sa_c.sin_family = hpc->h_addrtype;
		      
		      if((token3 = strtok(NULL, " "))){ // a port number was given
			if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			  sa_c.sin_port = atoi(token3);
			  rport_given = 1;
			} else{ // not a valid port
			  wprintw(sw[6], "Error: invalid port number.");
			}
		      }

		      // allocate open socket
		      if((sc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			wmove(sw[6], 0, 0);
			update_win(6);
			perror("socket");
		      }
		      
		      connect_sock(sc, &sa_c, sizeof sa_c);
		      f_dropr = 0;
		      f_droppedr = 0;
		      close_conn = 0;
		      remoteport = sa_c.sin_port;
		      
		      i3 = sizeof(saddr);
		      getsockname(sc, (struct sockaddr *)&saddr, (socklen_t *)&i3);
		      localport = (int)ntohs(saddr.sin_port);
		      
		    } else if(strcmp(token3, "connectl") == 0){
		      wprintw(sw[4], "Error: a head piggy can't use connectl.");
		     
		    } else if(strcmp(token3, "listenl") == 0){
		      wprintw(sw[4], "Error: a head piggy can't use listenl.");
		      
		    } else if(strcmp(token3, "listenr") == 0){
		      
		      for(i2 = 0; i2 <= max_sd; ++i2){
			if(FD_ISSET(i2, &master_set)){
			  close(i2);
			}
		      }
		    
		      client = 0;
		      server = 1;
		      
		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			sa_c.sin_port = atoi(token3);

			// allocate open socket
			if((scs = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			  wmove(sw[6], 0, 0);
			  update_win(6);
			  perror("socket");
			}

			bind_listen(scs, &sa_c, sizeof sa_c);
			if(scs > max_sd){
			  max_sd = scs;
			}
			FD_SET(scs, &master_set);
       
		      } else{ // not a valid port
			wprintw(sw[6], "Error: invalid port number.");
		      }

		    } else if(strcmp(token3, "read") == 0){
		      strncpy(filename2, strtok(NULL, " "), 30);
		      
		      if((file2 = fopen(filename2, "r")) != NULL){
			fgets(commands2, 300, file2);
			token2 = strtok(commands2, " ");
			if(client == 1){
			  send_sock = sc;
			} else {
			  send_sock = sc2;
			}
			
		      } else {
			wprintw(sw[0], "read: Invalid file to read.\n");
		      }
		      
		      
		    } else if(strcmp(token3, "llport") == 0){
		      wprintw(sw[4], "Error: a head piggy can't use llport.");
		      
		    } else if(strcmp(token3, "rlport") == 0){
		      if(client == 1){
		        send_sock = sc;
		      }else {
			send_sock = sc2;
		      }
		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			saddr.sin_port = atoi(token3);

		      } else {
			wprintw(sw[4], "Error: invalid port number.");
		      }	
		      
		    } else if(strcmp(token3, "lrport") == 0){
		      wprintw(sw[4], "Error: a head piggy can't use lrport.");

		    } else if(strcmp(token3, "lraddr") == 0){
		       wprintw(sw[4], "Error: a head piggy can't use lraddr.");
		      
		    } else if(strcmp(token3, "rraddr") == 0){
		      token3 = strtok(NULL, " ");
		      // Look up given hostname
		      if((hpc = gethostbyname(token3)) == NULL){
		        wprintw(sw[6], "%s: no such host?", host);
		      } else {
			if(client == 1){
			  // Put host's address and type into socket struct
			  bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
			  sa_c.sin_family = hpc->h_addrtype;
			} else {
			  do{
			    // Accept each incoming connection
			    i3 = sizeof sa_c2;
			    if((sc2 = accept(scs, &sa_c2, &i3)) < 0){
			      if(errno != EWOULDBLOCK){
				wmove(sw[6], 0, 0);
				update_win(6);
				perror("accept");
			      }
			    }
			    f_dropr = 0;
			    f_droppedr = 0;
			    close_conn = 0;
			    
			    remoteport = sa_c2.sin_port;
			    hpc2 = gethostbyaddr(&sa_c2.sin_addr, sizeof(sa_c2.sin_addr), AF_INET);
			    host = hpc2->h_name;

			    if(strcmp(token3, host) != 0){
			      f_dropr = 1;
			    } else {
			      FD_SET(sc2, &master_set);
			      if(sc2 > max_sd){
				max_sd = sc2;
			      }
			    }
			    
			  }while(f_dropr == 1 || f_droppedr == 1);
			  
			}
		      }
		    } 
		  
		  } else { // a short command was given, but it doesn't match anything
		    wclear(sw[4]);
		    for(i2 = 0; i2 < keys - 1; i2++){
		      wprintw(sw[4], "%c", input[i2]);
		    }
		    wprintw(sw[4], ": Unknown command");
		  }
		  
		  keys = 0;
		  cursor = 0;
		  input_change();
		  strncpy(&in1, &input, 1000);
		  update_all();
		  wmove(sw[4], 0, 0);
		  break;
	      } // end switch
	    } // end while
	    
	  } else {
	    close_conn = 0;
	    f_droppedr = 0;

	    if(i == scs && FD_ISSET(scs, &master_set)){
	      // Accept each incoming connection
	      i3 = sizeof sa_s2;
	      

	      f_dropr = 0;
	      f_droppedr = 0;
	      close_conn = 0;
	      remoteport = sa_c2.sin_port;
	      hpc2 = gethostbyaddr(&sa_c2.sin_addr, sizeof(sa_c2.sin_addr), AF_INET);
	      host = hpc2->h_name;
	      
	      FD_SET(sc2, &master_set);
	      if(sc2 > max_sd){
		max_sd = sc2;
	      }

	    } // end accepting block
	    
	    // Receive all incoming data on this socket
	    do{
	      
	      if(f_dropr == 0){
		close_conn = 0;
	      } else {
		close_conn = 1;
		f_droppedr = 1;
		remoteport = 0;
		break;
	      }
	      
	      // Clear incoming buffer
	      for(i2 = sizeof(buf_r2); i2 >= 0; i2--){
		buf_r2[i2] = '\0';
	      }
	      
	      // Receive on this connection until failure
	      if((rc = recv(i, &buf_r2, sizeof(buf_r2), 0)) < 0){
		if(errno != EWOULDBLOCK){
		  wmove(sw[6], 0, 0);
		  update_win(6);
		  perror("recv()");
		  close_conn = 1;
		}
		if(errno != EAGAIN){
		  break;
		}
		
	      } else if(rc != 0){
		wprintw(sw[3], &buf_r2);
		wprintw(sw[3], "\n");
		update_win(3);
	      }
	      
	      // Check if connection was closed by server
	      if(rc == 0){
		wmove(sw[6], 0, 0);
		update_win(6);
		printf("Server Connection closed.\n\r");
		close_conn = 1;
		f_droppedr = 1;
		remoteport = 0;
		break;
	      }

	      if(f_loopl == 1){ // send back to the right
		for(i2 = sizeof(buf_r); i2 >= 0; i2--){
		  buf_r[i2] = '\0';
		}
		strncpy(&buf_r, &buf_r2, sizeof(buf_r));
	      
		rc = send(i, &buf_r, sizeof(buf_r), 0);
		wprintw(sw[1], &buf_r);
		wprintw(sw[1], "\n");
		update_win(1);
		while(rc != sizeof(buf_r)){
		  select(max_sd + 1, &working_set, NULL, NULL, NULL);
		  rc = send(i, &buf_r, sizeof(buf_r), 0);
		}
	      } // end if f_loopl == 1

	      
	    } while(close_conn == 0);
	    
	    if(close_conn == 1){
	      close(i);
	      FD_CLR(i, &master_set);
	      if(i == max_sd){
		while(FD_ISSET(max_sd, &master_set) == 0){
		  max_sd -= 1;
		} // end while
	      } // end if
	    } // end "if(close_conn...)"
	    
	    
	  } // end for existing connection
	} // end "if(FD_ISSET)"
      } // end for loop
    } while(1);

    
    endwin();

  } // end head piggy

  //-------------------------------------------------------------------------------------------------------

  else if(middle == 1){
    serverl = 1;
    clientr = 1;
    
    /* // Place cursor at top corner of window 5 */
    wmove(sw[4], 0, 0);
    wclrtoeol(sw[4]);
    waddstr(sw[4], "Commands");
    update_win(4);      

    
    // middle piggy acting as a client-----------------------------------------------
    
    // allocate open socket
    if((sc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
      wmove(sw[6], 0, 0);
      update_win(6);
      perror("socket");
    }
    
    if(rport_given == 0){
      sa_c.sin_port = 36762;
    }

    connect_sock(sc, &sa_c, sizeof sa_c);

    remoteport = sa_c.sin_port;
    
    // middle piggy acting as server------------------------------------------------------------------------

    // Get our own host info
    gethostname(localhost, MAXHOSTNAME);
    if((hps = gethostbyname(localhost)) == NULL){
      wmove(sw[6], 0, 0);
      update_win(6);
      wprintw(sw[6], "%s: cannot get local host info?");
    }

    sa_s1.sin_port = 36762;
    localportl = sa_s1.sin_port;

    bcopy((char *)hps->h_addr, (char *)&sa_s1.sin_addr, hps->h_length);
    sa_s1.sin_family = hps->h_addrtype;

    // Allocate open socket for incoming connections
    if((ss1 = socket(hps->h_addrtype, SOCK_STREAM, 0)) < 0){
      wmove(sw[6], 0, 0);
      update_win(6);
      perror("socket");
    }

    // Set maximum connections
    if(listen(ss1, BACKLOG) < 0){
      wmove(sw[6], 0, 0);
      update_win(6);
      perror("listen");
    }
    
    do{
      if(f_droppedl == 1 && f_persl == 1){
	// Set maximum connections
	if(listen(ss1, BACKLOG) < 0){
	  wmove(sw[6], 0, 0);
	  update_win(6);
	  perror("listen");
	}
	f_dropl = 0;
	f_droppedl = 0;
      }
      
      if(f_droppedr == 1 && f_persr == 1){
	connect_sock(sc, &sa_c, sizeof sa_c);
	f_dropr = 0;
	f_droppedr = 0;
	remoteport = sa_c.sin_port;
      }
      
      // Initialize master fd_set
      FD_ZERO(&master_set);
      
      max_sd = ss1;

      if(sc > max_sd){
	max_sd = sc;
      }
      
      FD_SET(0, &master_set);
      FD_SET(ss1, &master_set);
      FD_SET(sc, &master_set);
      
      do{

	
	// Call select
	if((rc = select(max_sd + 1, &working_set, NULL, NULL, NULL)) < 0){
	  wmove(sw[6], 0, 0);
	  update_win(6);
	  perror("select");
	  break;
	}
	
	// Determine which descriptors are readable
	desc_ready = rc;
	
	for(i = 0; i <= max_sd && desc_ready > 0; ++i){
	  // Check if descriptor is ready
	  if(FD_ISSET(i, &working_set)){

	    if(i == 0){ // stdin is readable
	      close_conn = 0;
	      f_dropr = 0;
	      f_dropl = 0;

	      wmove(sw[4], 0, 0);
	      wclear(sw[4]);
	      waddstr(sw[4], "Command Mode");
	      update_win(4);
	      
	      keys = 0; // index for stdin buffer
	      cursor = 0;
	      
	      while(1){
		switch(ci){
		  
		case 113: // q
		  // Quitting
		  nocbreak();

		  endwin();
		  exit(0);
		  
		case 105: // i
		  // Insert Mode
		  wmove(sw[5], 0, 0);
		  wclrtoeol(sw[5]);
		  wprintw(sw[5], "Insert Mode\n");
		  update_win(5);
		  
		  outkeys = 0; // index for outgoing buffer
		  
		  while(c != esc){
		    
		    switch(c){
		    case esc: // ESC
		      outkeys = 0;
		      buf_l[0] = '\0';
		      buf_r[0] = '\0';

		      // Back to Command Mode
		      wmove(sw[4], 0, 0);
		      wprintw(sw[4], "Command Mode: ");
		      update_win(4);
		      
		      break;
		      
		    case 13: // RET
		      buf_l[outkeys] = '\0';
		      buf_r[outkeys] = '\0';
		      
		      // send buffer to outgoing socket
		      if(f_outl == 1) { // going left

			if(serverl == 1){
			  send_sock = ss2;
			} else {
			  send_sock = ssc;
			}
			
			if(send(send_sock, &buf_l, sizeof(buf_l), 0) != sizeof(buf_l)){
			  wmove(sw[6], 0, 0);
			  update_win(6);
			  perror("send to left");
			  break;
			}
			
		        wprintw(sw[2], "\n");
			wprintw(sw[2], &buf_l);
			update_win(2);
			
		      } else {          // going right

			if(clientr == 1){
			  send_sock = sc;
			} else {
			  send_sock = sc2;
			}
			
			if(send(send_sock, &buf_r, sizeof(buf_r), 0) != sizeof(buf_r)){
			  wmove(sw[6], 0, 0);
			  update_win(6);
			  wprintw(sw[6], "send to right");
			  break;
			}
			
			wprintw(sw[1], "\n");
			wprintw(sw[1], &buf_r);
			update_win(1);
		      }
		      buf_l[0] = '\0';
		      buf_r[0] = '\0';
		      outkeys = 0;
		      break;
		      
		    case KEY_BACKSPACE: // Backspace
		    case KEY_DC:
		    case 127:
		      outkeys--;
		      if(outkeys < 0){
			outkeys = 0;
		      }
		      buf_l[outkeys] = '\0';
		      buf_r[outkeys] = '\0';
		      wclear(sw[5]);

		      for(i2 = 0; i2 <= outkeys; i2++){
			if(f_outl == 1){
			  wprintw(sw[5], "%c", buf_l[i2]);
			} else {
			  wprintw(sw[5], "%c", buf_r[i2]);
			}
		      }
		      update_win(5);
		      
		      break;
		      
		    default:
		      if(f_outl == 1) { // going left
			buf_l[outkeys] = c;
		      } else {          // going right
			buf_r[outkeys] = c;
		      }
		      outkeys++;
		      wclear(sw[5]);
		      update_win(5);

		      for(i2 = 0; i2 < outkeys; i2++){
			if(f_outl == 1){
			  wprintw(sw[5], "%c", buf_l[i2]);
			} else {
			  wprintw(sw[5], "%c", buf_r[i2]);
			}
		      }
		      
		    } // end switch
		  } // ESC was pressed, end insert mode

		default:
		  wclear(sw[4]);
		  if(ci != 105){ // lose 'i' from previous insert mode
		    input[cursor] = ci;
		    wprintw(sw[4], "%c|", ci);
		    keys++;
		    cursor++;
		    wmove(sw[4], 0, cursor);
		    update_win(4);
		  }
		  
		  while(ci != 13){ // Return
		    
		    switch(ci){
		      
		      // Backspace
		    case KEY_BACKSPACE:
		    case KEY_DC:
		    case 127:
		      
		      keys--;
		      if(keys < 0){
			keys = 0;
		      }
		      cursor--;
		      if(cursor < 0){
			cursor = 0;
		      }
		      wclear(sw[4]);
		      
		      if(cursor == keys){
			input[cursor] = '\0';
			
			for(i2 = 0; i2 <= cursor; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
			
		      } else {
			// reset the buffer
			for(i2 = cursor; i2 < keys; i2++){
			  input[i2] = input[i2 + 1];
			}
			input[keys + 1] = '\0';
			input[keys] = '\0';
			
			for(i2 = 0; i2 <= keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}

			wmove(sw[4], 0, cursor);
			update_win(4);
		      }
		      break;
		      
		    case esc:
		      switch(sw[4]){
			
		      case 'A': // Up arrow
			wclear(sw[4]);
			command_num++;
			if(command_num > 10){
			  command_num = 1;
			}
			command_nums();
			wprintw(sw[4], "%s", &input);
			update_win(4);
			break;

		      case 'B': // Down arrow
			wclear(sw[4]);
			command_num--;
			if(command_num < 1){
			  command_num = 10;
			}
			command_nums();
			wprintw(sw[4], "%s", &input);
			update_win(4);
			break;
			
		      case 'D': // Left arrow
			wclear(sw[4]);
			cursor--;
			if(cursor < 0){
			  cursor = 0;
			}
			
			for(i2 = 0; i2 < keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
			wmove(sw[4], 0, cursor);
			update_win(4);
			break;

		      case 'C': // right arrow
			wclear(sw[4]);
			cursor++;
			if(cursor > keys){
			  cursor = keys;
			}
			for(i2 = 0; i2 < keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
			wmove(sw[4], 0, cursor);
			update_win(4);
			break;

		      case 'F': // END KEY
			cursor = keys;
			wmove(sw[4], 0, cursor);
			update_win(4);
			break;
			
		      case 'H': // HOME KEY
			cursor = 0;
			wmove(sw[4], 0, cursor);
			update_win(4);
			break;
			
		      } // end esc switch
		      break;

		    case '~': // DELETE KEY
		      if(keys != cursor){
			wclear(sw[4]);
			for(i2 = cursor; i2 < keys - 1; i2++){
			  input[i2] = input[i2 + 1];
			}
			input[keys - 1] = '\0';
			keys--;
			if(keys < 0){
			  keys = 0;
			}
			cursor--;
			if(cursor < 0){
			  cursor = 0;
			}
			wprintw(sw[4], 0, cursor);
			update_win(4);
		      }
		      break;

		    case 13: // RETURN
		      wclear(sw[4]);
		      if(command_num == 0){
			input[keys] = ci;
		      }
		      keys++;
		      cursor++;
		      for(i2 = 0; i2 < keys; i2++){
			wprintw(sw[4], "%c", input[i2]);
		      }
		      wmove(sw[4], 0, cursor);
		      update_win(4);

		      break;
		      
		  default:
		    wclear(sw[4]);
		    if(cursor != keys){
		      for(i2 = keys; i2 > cursor; i2--){
		        input[i2] = input[i2 - 1];
		      }
		    }
		    
		    input[cursor] = ci;
		    keys++;
		    cursor++;

		    for(i2 = 0; i2 < keys; i2++){
		      wprintw(sw[4], "%c", input[i2]);
		    }
		    wmove(sw[4], 0, cursor);
		    update_win(4);
		    
		    } // end switch
		  } // Return was pressed

		  wclear(sw[4]);
		  if(command_num == 0){
		    input[keys] = '\0'; // Null terminate
		  }
		  command_num = 0;
		  
		  if(strcmp(input, "output\r") == 0){
		    if(f_outl == 0){ // middle piggies send to the right by default
		      wprintw(sw[4], "output: Left to right");
		    } else{ // f_outl == 1
		      wprintw(sw[4], "output: Right to left");
		    }
		  
		  } else if(strcmp(input, "outputl\r") == 0){
		    f_outl = 1;
		    f_outr = 0;
		    wprintw(sw[4], "outputl: Output direction is to the left.");
		    
		  } else if(strcmp(input, "outputr\r") == 0){
		    f_outr = 1;
		    f_outl = 0;
		    wprintw(sw[4], "outputr: Output direction is to the right.");
		    
		  } else if(strcmp(input, "dropr\r") == 0){
		    f_dropr = 1;

		    if(clientr == 1){
		      if(FD_ISSET(sc, &master_set)){
			close(sc);
			FD_CLR(sc, &master_set);
		      }
		    } else {
		      if(FD_ISSET(scs, &master_set)){
			close(scs);
			FD_CLR(scs, &master_set);
		      }
		      if(FD_ISSET(sc2, &master_set)){
			close(sc2);
			FD_CLR(sc2, &master_set);
		      }
		    }
		    wprintw(sw[4], "dropr: dropping the right side connection.");

		  } else if(strcmp(input, "dropl\r") == 0){
		    f_dropl = 1;
		    if(serverl == 1){
		      if(FD_ISSET(ss1, &master_set)){
			close(ss1);
			FD_CLR(ss1, &master_set);
		      }
		      if(FD_ISSET(ss2, &master_set)){
			close(ss2);
			FD_CLR(ss2, &master_set);
		      }
		    } else {
		      if(FD_ISSET(ssc, &master_set)){
			close(ssc);
			FD_CLR(ssc, &master_set);
		      }
		    }
		    wprintw(sw[4], "dropl: dropping the left side connection.");
		    
		  } else if(strcmp(input, "persl\r") == 0){
		    wprintw(sw[4], "persl: Left connection is persistent.");
		    
		  } else if(strcmp(input, "persr\r") == 0){
		    wprintw(sw[4], "persr: Right connection is persistent.");
		    
		  } else if(strcmp(input, "right\r") == 0){
		    if(remoteport == 0 || f_droppedr == 1 || f_dropr == 1){
		      wprintw(sw[4], "Right connection: %s:%d:*:*\tNONE", localhost, localport);
		    } else{
		      wprintw(sw[4], "Right connection: %s:%d:%s:%d\t", localhost, localport, host, remoteport);
		      if(remoteport != 0){
		        wprintw(sw[4], "CONNECTED");
		      } else {
		        wprintw(sw[4], "LISTENING");
		      }
		    }
		  
		  } else if(strcmp(input, "left\r") == 0){
		    if(remoteportl == 0 || f_droppedl == 1 || f_dropl == 1){
		      wprintw(sw[4], "Left connection: *:*:%s:%d\tNONE", localhost, localportl);
		    } else {
		      wprintw(sw[4], "Left connection: %s:%d:%s:%d\t", hostl, remoteportl, localhost, localportl);
		      if(remoteportl != 0){
		        wprintw(sw[4], "CONNECTED");
		      } else {
		        wprintw(sw[4], "LISTENING");
		      }
		    }
		    
		  } else if(strcmp(input, "loopr\r") == 0){
		    f_loopr = 1;
		    wprintw(sw[4], "loopr: Rightward data will go left.");
		      
		  } else if(strcmp(input, "loopl\r") == 0){
		    f_loopl = 1;
		    wprintw(sw[4], "loopl: Leftward data will go right.");

		  } else if(strcmp(input, "listenl\r") == 0){
		    if(clientl == 1){
		      if(FD_ISSET(ssc, &master_set)){
			close(ssc);
			FD_CLR(ssc, &master_set);
		      }
		    } else {
		      if(FD_ISSET(ss1, &master_set)){
			close(ss1);
			FD_CLR(ss1, &master_set);
		      }
		      if(FD_ISSET(ss2, &master_set)){
			close(ss2);
			FD_CLR(ss2, &master_set);
		      }
		    }

		    serverr = 0;
		    clientl = 0;
		    serverl = 1;
		    clientr = 1;

		    sa_s1.sin_port = 36762;

		    // allocate open socket
		    if((ss1 = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("socket");
		    }
		    
		    bind_listen(ss1, &sa_s1, sizeof sa_s1);
		    if(ss1 > max_sd){
		      max_sd = ss1;
		    }
		    FD_SET(ss1, &master_set);
		    
		  } else if(strcmp(input, "listenr\r") == 0){
		    if(clientr == 1){
		      if(FD_ISSET(sc, &master_set)){
			close(sc);
			FD_CLR(sc, &master_set);
		      }
		    } else {
		      if(FD_ISSET(scs, &master_set)){
			close(scs);
			FD_CLR(scs, &master_set);
		      }
		      if(FD_ISSET(sc2, &master_set)){
			close(sc2);
			FD_CLR(sc2, &master_set);
		      }
		    }
		    
		    clientr = 0;
		    serverl = 0;
		    clientl = 1;
		    serverr = 1;
		    sa_s1.sin_port = 36762;
		      
		    // allocate open socket
		    if((scs = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("socket");
		    }
		    
		    bind_listen(scs, &sa_s1, sizeof sa_s1);
		    if(scs > max_sd){
		      max_sd = scs;
		    }
		    FD_SET(scs, &master_set);
		  
		  } else if(strcmp(input, "reset\r") == 0){
		    reset();

		  } else if((keys - 1) > 7){ // a long command (with parameters) was called

		    strncpy(&input2, &input, 1000);
		    token3 = strtok(input2, " ");
		    
		    if(strcmp(token3, "connectr") == 0){
		      if(clientr == 1){
			if(FD_ISSET(sc, &master_set)){
			  close(sc);
			  FD_CLR(sc, &master_set);
			}
		      } else {
			if(FD_ISSET(scs, &master_set)){
			  close(scs);
			  FD_CLR(scs, &master_set);
			}
			if(FD_ISSET(sc2, &master_set)){
			  close(sc2);
			  FD_CLR(sc2, &master_set);
			}
		      }
		      clientl = 0;
		      serverr = 0;
		      clientr = 1;
		      serverl = 1;
		      
		      token3 = strtok(NULL, " ");
		      host = token3;

		      // Look up given hostname
		      if((hpc = gethostbyname(host)) == NULL){
		        wprintw(sw[0], "%s: no such host?\n", host);
		        break;
		      }

		      // Put host's address and type into socket struct
		      bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
		      sa_c.sin_family = hpc->h_addrtype;
		      
		      if((token3 = strtok(NULL, " "))){ // a port number was given
			if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			  sa_c.sin_port = atoi(token3);
			  rport_given = 1;
			} else{ // not a valid port
			  wprintw(sw[6], "Error: invalid port number.");
			}
		      } else { // a port number was not given
			if(rport_given == 0){
			  sa_c.sin_port = 36762;
			}
		      }

		      // allocate open socket
			if((sc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			  wmove(sw[6], 0, 0);
			  update_win(6);
			  perror("socket");
			}
		      
		      connect_sock(sc, &sa_c, sizeof sa_c);
		      f_dropr = 0;
		      f_droppedr = 0;
		      remoteport = sa_c.sin_port;
		    
		    } else if(strcmp(token3, "connectl") == 0){
		      if(clientl == 1){
			if(FD_ISSET(ssc, &master_set)){
			  close(ssc);
			  FD_CLR(ssc, &master_set);
			}
		      } else {
			if(FD_ISSET(ss1, &master_set)){
			  close(ss1);
			  FD_CLR(ss1, &master_set);
			}
			if(FD_ISSET(ss2, &master_set)){
			  close(ss2);
			  FD_CLR(ss2, &master_set);
			}
		      }
		      clientr = 0;
		      serverl = 0;
		      clientl = 1;
		      serverr = 1;
		      
		      token3 = strtok(NULL, " ");
		      host = token3;

		      
		      // Look up given hostname
		      if((hpc = gethostbyname(host)) == NULL){
		        wprintw(sw[0], "%s: no such host?\n", host);
		        break;
		      }

		      // Put host's address and type into socket struct
		      bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
		      sa_c.sin_family = hpc->h_addrtype;
		      
		      
		      if((token3 = strtok(NULL, " "))){ // a port number was given
			if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			  sa_c.sin_port = atoi(token3);
			  lport_given = 1;
			} else{ // not a valid port
			  wprintw(sw[6], "Error: invalid port number.");
			}
		      } else { // a port number was not given
			if(lport_given == 0){
			  sa_c.sin_port = 36762;
			}
		      }

		      // allocate open socket
		      if((ssc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			wmove(sw[6], 0, 0);
			update_win(6);
			perror("socket");
		      }
		      
		      connect_sock(ssc, &sa_c, sizeof sa_c);
		      f_dropl = 0;
		      f_droppedl = 0;
		      remoteportl = sa_c.sin_port;
		    
		      
		    } else if(strcmp(token3, "listenl") == 0){
		      if(clientl == 1){
			if(FD_ISSET(ssc, &master_set)){
			  close(ssc);
			  FD_CLR(ssc, &master_set);
			}
		      } else {
			if(FD_ISSET(ss1, &master_set)){
			  close(ss1);
			  FD_CLR(ss1, &master_set);
			}
			if(FD_ISSET(ss2, &master_set)){
			  close(ss2);
			  FD_CLR(ss2, &master_set);
			}
		      }
		      clientl = 0;
		      serverr = 0;
		      clientr = 1;
		      serverl = 1;

		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			sa_s1.sin_port = atoi(token3);

			
			// allocate open socket
			if((ss1 = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			  wmove(sw[6], 0, 0);
			  update_win(6);
			  perror("socket");
			}
		      
			
			bind_listen(ss1, &sa_s1, sizeof sa_s1);
			localportl = sa_s1.sin_port;
			if(ss1 > max_sd){
			  max_sd = ss1;
			}
			FD_SET(ss1, &master_set);
       
		      } else{ // not a valid port
			wprintw(sw[6], "Error: invalid port number.");
		      }
		    		      
		    } else if(strcmp(token3, "listenr") == 0){
		      if(clientr == 1){
			if(FD_ISSET(sc, &master_set)){
			  close(sc);
			  FD_CLR(sc, &master_set);
			}
		      } else {
			if(FD_ISSET(scs, &master_set)){
			  close(scs);
			  FD_CLR(scs, &master_set);
			}
			if(FD_ISSET(sc2, &master_set)){
			  close(sc2);
			  FD_CLR(sc2, &master_set);
			}
		      }
		      
		      clientr = 0;
		      serverl = 0;
		      clientl = 1;
		      serverr = 1;
		      
		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			sa_c.sin_port = atoi(token3);
			remoteport = sa_c.sin_port;
			// allocate open socket
			if((scs = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			  wmove(sw[6], 0, 0);
			  update_win(6);
			  perror("socket");
			}

			bind_listen(scs, &sa_c, sizeof sa_c);
			if(scs > max_sd){
			  max_sd = scs;
			}
			FD_SET(scs, &master_set);
       
		      } else{ // not a valid port
			wprintw(sw[6], "Error: invalid port number.");
		      }

		    } else if(strcmp(token3, "read") == 0){
		      strncpy(filename2, strtok(NULL, " "), 30);
		      
		      if((file2 = fopen(filename2, "r")) != NULL){
			fgets(commands2, 300, file2);
			token2 = strtok(commands2, " ");
			if(f_outl == 0){ // go right
			  if(clientr == 1){
			    send_sock = sc;
			  } else {
			    send_sock = sc2;
			  }
			} else {// go left
			  if(serverl == 1){
			    send_sock = ss2;
			  } else {
			    send_sock = ssc;
			  }
			}
			while(token2){
			  
			  send(send_sock, token2, sizeof(token2), 0);
			  send(send_sock, space, sizeof(space), 0);
			  if(f_outl == 0){ // go right
			    wprintw(sw[1], token2);
			    wprintw(sw[1], " ");
			    update_win(1);
			  } else { // go left
			    wprintw(sw[2], token2);
			    wprintw(sw[2], " ");
			    update_win(2);
			  }
			  token2 = strtok(NULL, " ");
			}
		      } else {
			wprintw(sw[0], "read: Invalid file to read.\n");
		      }
		      
		      
		    } else if(strcmp(token3, "llport") == 0){
		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			localportl = atoi(token3);
			if(clientl == 1){
			  saddr.sin_port = atoi(token3);
			  
			  //Bind socket to service port
			  if(bind(ssc, &saddr, sizeof saddr) < 0){		
			    perror("bind");
			  }
			} else {
			  sa_s1.sin_port = atoi(token3);
			  
			  //Bind socket to service port
			  if(bind(ss1, &saddr, sizeof saddr) < 0){		
			    perror("bind");
			  }
			}
		      } else { // not a valid port
			wprintw(sw[4], "Error: invalid port number.");
		      }

		    } else if(strcmp(token3, "rlport") == 0){

		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			localport = atoi(token3);
			if(clientr == 1){
			  saddr.sin_port = atoi(token3);

			  //Bind socket to service port
			  if(bind(sc, &saddr, sizeof saddr) < 0){		
			    perror("bind");
			  }
			} else {
			  sa_s1.sin_port = atoi(token3);
			  
			  //Bind socket to service port
			  if(bind(scs, &saddr, sizeof saddr) < 0){		
			    perror("bind");
			  }
			}
		      } else {
			wprintw(sw[4], "Error: invalid port number.");
		      }
		      
		    } else if(strcmp(token3, "lrport") == 0){
		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			if(clientl == 1){
			  sa_c.sin_port = atoi(token3);
			  remoteportl = atoi(token3);
			} else{
			  do{
			    // Accept each incoming connection
			    i3 = sizeof sa_s2;
			    if((ss2 = accept(ss1, &sa_s2, &i3)) < 0){
			      if(errno != EWOULDBLOCK){
				wmove(sw[6], 0, 0);
				update_win(6);
				perror("accept");
			      }
			    }
			    f_dropl = 0;
			    f_droppedl = 0;

			    remoteportl = sa_s2.sin_port;
			    hps2 = gethostbyaddr(&sa_s2.sin_addr, sizeof(sa_s2.sin_addr), AF_INET);
			    hostl = hpc2->h_name;

			    if(atoi(token3) != sa_s2.sin_port){
			      f_dropl = 1;
			    } else {
			      FD_SET(ss2, &master_set);
			      if(ss2 > max_sd){
				max_sd = ss2;
			      }
			    }
			    
			  }while(f_dropl == 1 || f_droppedl == 1);

			}
		      } else {
			wprintw(sw[4], "Error: invalid port number.");
		      }

		    } else if(strcmp(token3, "lraddr") == 0){
		      token3 = strtok(NULL, " ");

		      // Look up given hostname
		      if((hps2 = gethostbyname(token3)) == NULL){
		        wprintw(sw[6], "%s: no such host?", token3);
		      } else {
			if(clientl == 1){
			  // Put host's address and type into socket struct
			  bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
			  sa_c.sin_family = hpc->h_addrtype;
			} else {
			  do{
			    // Accept each incoming connection
			    i3 = sizeof sa_s2;
			    if((ss2 = accept(ss1, &sa_s2, &i3)) < 0){
			      if(errno != EWOULDBLOCK){
				wmove(sw[6], 0, 0);
				update_win(6);
				perror("accept");
			      }
			    }
			    f_dropl = 0;
			    f_droppedl = 0;

			    remoteportl = sa_s2.sin_port;
			    hpc2 = gethostbyaddr(&sa_s2.sin_addr, sizeof(sa_s2.sin_addr), AF_INET);
			    hostl = hpc2->h_name;

			    if(strcmp(token3, hostl) != 0){
			      f_dropl = 1;
			    } else {
			      FD_SET(ss2, &master_set);
			      if(ss2 > max_sd){
				max_sd = ss2;
			      }
			    }
			    
			  }while(f_dropl == 1 || f_droppedl == 1);
			  
			}
		      }
		      
		    } else if(strcmp(token3, "rraddr") == 0){
		      token3 = strtok(NULL, " ");
		      // Look up given hostname
		      if((hpc = gethostbyname(token3)) == NULL){
		        wprintw(sw[6], "%s: no such host?", token3);
		      } else {
			if(clientr == 1){
			  // Put host's address and type into socket struct
			  bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
			  sa_c.sin_family = hpc->h_addrtype;
			} else {
			  do{
			    // Accept each incoming connection
			    i3 = sizeof sa_s2;
			    if((sc2 = accept(scs, &sa_c2, &i3)) < 0){
			      if(errno != EWOULDBLOCK){
				wmove(sw[6], 0, 0);
				update_win(6);
				perror("accept");
			      }
			    }
			    f_dropr = 0;
			    f_droppedr = 0;

			    remoteport = sa_c2.sin_port;
			    hpc2 = gethostbyaddr(&sa_c2.sin_addr, sizeof(sa_c2.sin_addr), AF_INET);
			    host = hpc2->h_name;

			    if(strcmp(token3, host) != 0){
			      f_dropr = 1;
			    } else {
			      FD_SET(sc2, &master_set);
			      if(sc2 > max_sd){
				max_sd = sc2;
			      }
			    }
			    
			  }while(f_dropr == 1 || f_droppedr == 1);
			  
			}
		      }
		    } else { // a long command was given, but it doesn't match anything
		      wclear(sw[4]);
		      for(i2 = 0; i2 < keys - 1; i2++){
			wprintw(sw[4], "%c", input[i2]);
		      }
		      wprintw(sw[4], ": Unknown command");
		    }
		  
		  } else { // a short command was given, but it doesn't match anything
		    wclear(sw[4]);
		    for(i2 = 0; i2 < keys - 1; i2++){
		      wprintw(sw[4], "%c", input[i2]);
		    }
		    wprintw(sw[4], ": Unknown command");
		  }

		    //
		  keys = 0;
		  cursor = 0;
		  input_change();
		  strncpy(&in1, &input, 1000);
		  break;
		} // end switch
	      } // end while
	    
	    } else if(i == ss1){ // this listening socket is readable
	      // Accept each incoming connection
	      i3 = sizeof sa_s2;
	      if((ss2 = accept(ss1, &sa_s2, &i3)) < 0){
		if(errno != EWOULDBLOCK){
		  wmove(sw[6], 0, 0);
		  update_win(6);
		  perror("accept");
		  end_server = 1;
		}
		break;
	      }
	      
	      remoteportl = sa_s2.sin_port;
	      hps2 = gethostbyaddr(&sa_s2.sin_addr, sizeof(sa_s2.sin_addr), AF_INET);
	      hostl = hps2->h_name;
	      
	      FD_SET(ss2, &master_set);
	      if(ss2 > max_sd){
		max_sd = ss2;
	      }

	    } else if(i == scs){ // this listening socket is readable
	      // Accept each incoming connection
	      i3 = sizeof sa_c2;
	      if((sc2 = accept(scs, &sa_c2, &i3)) < 0){
		if(errno != EWOULDBLOCK){
		  wmove(sw[6], 0, 0);
		  update_win(6);
		  perror("accept");
		  end_server = 1;
		}
		break;
	      }
	      
	      remoteport = sa_c2.sin_port;
	      hpc2 = gethostbyaddr(&sa_c2.sin_addr, sizeof(sa_c2.sin_addr), AF_INET);
	      host = hpc2->h_name;
	      
	      FD_SET(sc2, &master_set);
	      if(sc2 > max_sd){
		max_sd = sc2;
	      }
	    
	      
	    } else { // this is not the listening socket
	      close_conn = 0;
	      f_dropr = 0;
	      f_dropl = 0;
	      f_droppedr = 0;
	      f_droppedl = 0;
	      
	      do{
		
		// check if connection is from the left
		if(i != sc && i != sc2){
		  
		  if(f_dropl == 1){ // drop connection
		    close_conn = 1;
		    f_droppedl = 1;
		    break;
		  }
		  

		  for(i2 = sizeof(buf_l2); i2 >= 0; i2--){
		    buf_l2[i2] = '\0';
		  }
		  // Receive data on connection until fails
		  if((rc = recv(i, &buf_l2, sizeof(buf_l2), 0)) < 0){
		    if(errno != EWOULDBLOCK){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("recv() from left");
		      close_conn = 1;
		    }
		    if(errno != EAGAIN){
		      break;
		    }
		  }
		  wprintw(sw[0], &buf_l2);
		  wprintw(sw[0], "\n");
		  update_win(0);
		  
		  buf_l2[rc] = '\0';
		  
		  // Check if connection was closed by client
		  if(rc == 0){
		    wmove(sw[6], 0, 0);
		    update_win(6);
		    printf("Left Connection closed.\n\r");
		    close_conn = 1;
		    f_droppedl = 1;
		    break;
		  }
		  
		  if(f_loopr == 0){ // send to the right

		    if(clientr == 1){
		      send_sock = sc;
		    } else {
		      send_sock = ss2;
		    }

		    buf_r[0] = '\0';
		    strncpy(&buf_r, &buf_l2, sizeof(buf_l2));
		    if(send(send_sock, &buf_r, strlen(&buf_r), 0) != strlen(&buf_r)){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      wprintw(sw[6], "send error");
		    } else {
		      wprintw(sw[1], &buf_r);
		      wprintw(sw[1], "\n");
		      update_win(1);
		    }
		  } else { // send back to the left
		    
		    buf_l[0] = '\0';
		    strncpy(&buf_l, &buf_l2, sizeof(buf_l2));
		    if(send(i, &buf_l, strlen(&buf_l), 0) != strlen(&buf_l)){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("send to left");
		    } else {
		      wprintw(sw[2], &buf_l);
		      wprintw(sw[2], "\n");
		      update_win(2);
		    }
		  }
		  
		} else { // connection was from the right
		  
		  if(f_dropr == 1){
		    close_conn = 1;
		    f_droppedr = 1;
		    remoteport = 0;
		    break;
		  }

		  buf_r2[0] = '\0';
		  // Receive data on connection until fails
		  if((rc = recv(i, &buf_r2, sizeof(buf_r2), 0)) < 0){
		    if(errno != EWOULDBLOCK){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("recv() from right");
		      close_conn = 1;
		    }
		    if(errno != EAGAIN){
		      break;
		    } 
		  } else {
		    wprintw(sw[3], &buf_r2);
		    wprintw(sw[3], "\n");
		    update_win(3);
		  }
		  buf_r2[rc] = '\0';
		  
		  // Check if connection was closed by server
		  if(rc == 0){
		    wmove(sw[6], 0, 0);
		    update_win(6);
		    printf("Right Connection closed.\n\r");
		    close_conn = 1;
		    f_droppedr = 1;
		    remoteport = 0;
		    break;
		  }
		  
		  if(f_loopl == 0){ // send to the left

		    if(clientl == 1){
		      send_sock = ssc;
		    } else {
		      send_sock = ss2;
		    }
		    
		    buf_l[0] = '\0';
		    strncpy(&buf_l, &buf_r2, sizeof(buf_r2));
		    if(send(send_sock, &buf_l, strlen(&buf_l), 0) != strlen(&buf_l)){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("send to left");
		      exit(1);
		    } else {
		      wprintw(sw[2], &buf_l);
		      wprintw(sw[2], "\n");
		      update_win(2);
		    }
		  } else { // send back to right
		    buf_r[0] = '\0';
		    strncpy(&buf_r, &buf_r2, sizeof(buf_r2));
		    if(send(i, &buf_r, strlen(&buf_r), 0) != strlen(&buf_r)){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      wprintw(sw[6], "send error");
		    } else {
		      wprintw(sw[1], &buf_r);
		      wprintw(sw[1], "\n");
		      update_win(1);
		    }
		  }
		}
		
	      } while(1 && close_conn == 0);	    
	      if(close_conn == 1){
		close(i);
		FD_CLR(i, &master_set);
		if(i == max_sd){
		  while(FD_ISSET(max_sd, &master_set) == 0){
		    max_sd -= 1;
		  }
		}
	      }
	    }
	  }
	} // end for loop
      } while(end_server == 0);
    } while(f_persl == 1 || f_persr == 1);
    
  }// end middle piggy

  //--------------------------------------------
  if(tail == 1){ // tail piggy

    server = 1;
    
    // Place cursor at top corner of window 5
    wmove(sw[4], 0, 0);
    wclrtoeol(sw[4]);
    waddstr(sw[4], "Commands");
    update_win(4);      
    
    // Allocate open socket for incoming connections
    if((ss1 = socket(hps->h_addrtype, SOCK_STREAM, 0)) < 0){
      wmove(sw[6], 0, 0);
      update_win(6);
      perror("socket");
    }
    
    // Bind socket to service port
    if(bind(ss1, &sa_s1, sizeof sa_s1) < 0){
      wmove(sw[6], 0, 0);
      update_win(6);
      perror("bind");
    }
    
    do{
      // Set maximum connections
      if(listen(ss1, BACKLOG) < 0) {
	wmove(sw[6], 0, 0);
	update_win(6);
	perror("listen");
      }

      localport = sa_s1.sin_port;
      
      // Initialize master fd_set
      FD_ZERO(&master_set);
      max_sd = ss1;

      if(ssc > max_sd){
	max_sd = ssc;
      }
      
      FD_SET(0, &master_set);
      FD_SET(ss1, &master_set);
      FD_SET(ssc, &master_set);
      
      do{
	
	// Call select
	if((rc = select(max_sd + 1, &working_set, NULL, NULL, NULL)) < 0){
	  wmove(sw[6], 0, 0);
	  update_win(6);
	  perror("select");
	  break;
	}
	
	// Determine which descriptors are readable
	desc_ready = rc;
	
	for(i = 0; i <= max_sd && desc_ready > 0; ++i){
	  // Check if descriptor is ready
	  if(FD_ISSET(i, &working_set)){
	    
	    desc_ready -= 1;
	    
	    if(i == 0){ // stdin is readable
	      close_conn = 0;
	      keys = 0; // index for stdin buffer
	      cursor = 0;
	      
	      while(1){
		switch(ci){
		  
		case 113: // q
		  // Quitting
		  nocbreak();
		  
		  endwin();
		  exit(0);
		  
		case 105: // i
		  // Insert Mode
		  wmove(sw[5], 0, 0);
		  wclrtoeol(sw[5]);
		  wprintw(sw[5], "Insert Mode\n");
		  update_win(5);
		  
		  outkeys = 0; // index for outgoing buffer
		  
		  while(c != esc){   
		    case 13: // RET
		      
		      buf_l[outkeys] = '\0';

		      if(server == 1){
			send_sock = ss2;
		      } else {
			send_sock = ssc;
		      }
		      break;
                    
		    case 127: // Backspace 
		      outkeys--;
		      if(outkeys < 0){
			outkeys = 0;
		      }
		      
		      buf_l[outkeys] = '\0';
		      wclear(sw[5]);

		      for(i2 = 0; i2 <= outkeys; i2++){
			wprintw(sw[5], "%c", buf_l[i2]);
		      }
		      update_win(5);
		      
		      break;
		      
		    default:
		      buf_l[outkeys] = c;
		      outkeys++;
		      wclear(sw[5]);
		      update_win(5);

		      for(i2 = 0; i2 < outkeys; i2++){
			wprintw(sw[5], "%c", buf_l[i2]);
		      }
		    } // end switch
		  } // ESC was pressed, end insert mode
		  
		default:
		  wclear(sw[4]);
		  if(ci != 105){ // lose 'i' from previous insert mode
		    input[keys] = ci;
		    wprintw(sw[4], "%c", ci);
		    keys++;
		    cursor++;
		    wmove(sw[4], 0, cursor);
		    update_win(4);
		  }
		  
		  while(ci != 13){ // Return		    
		    switch(ci){
		      
		      // Backspace
		  case KEY_BACKSPACE:
		    case KEY_DC:
		    case 127:
		      
		      keys--;
		      if(keys < 0){
			keys = 0;
		      }
		      cursor--;
		      if(cursor < 0){
			cursor = 0;
		      }

		      wclear(sw[4]);
		      
		      if(cursor == keys){
			input[cursor] = '\0';
			
			for(i2 = 0; i2 <= cursor; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
			
		      } else {
			// reset the buffer
			for(i2 = cursor; i2 < keys; i2++){
			  input[i2] = input[i2 + 1];
			}
			input[keys + 1] = '\0';
			input[keys] = '\0';
			
			for(i2 = 0; i2 <= keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
			wmove(sw[4], 0, cursor);
			update_win(4);
		      }
		      break;
		      
		    case esc:			
		      case 'A': // Up arrow
			wclear(sw[4]);
			command_num++;
			if(command_num > 10){
			  command_num = 1;
			}
			command_nums();
			wprintw(sw[4], "%s", &input);
			update_win(4);
			break;

		      case 'B': // Down arrow
			wclear(sw[4]);
			command_num--;
			if(command_num < 1){
			  command_num = 10;
			}
			command_nums();
			wprintw(sw[4], "%s", &input);
			update_win(4);
			break;
			
		      case 'D': // Left arrow
			wclear(sw[4]);
			cursor--;
			if(cursor < 0){
			  cursor = 0;
			}
			for(i2 = 0; i2 < keys; i2++){
			  wprintw(sw[4], "%c", input[i2]);
			}
			wmove(sw[4], 0, cursor);
			update_win(4);
			break;
			

		      case 'F': // END KEY
			cursor = keys;
		      	wmove(sw[4], 0, cursor);
			update_win(4);
			break;

		      case 'H': // HOME KEY
			cursor = 0;
			wmove(sw[4], 0, cursor);
			update_win(4);
			break;
			
		      } // end esc switch
		    break;

		    case 13: // RETURN
		      wclear(sw[4]);
		      if(command_num == 0){
			input[keys] = ci;
		      }
		      keys++;
		      cursor++;
		      for(i2 = 0; i2 < keys; i2++){
			wprintw(sw[4], "%c", input[i2]);
		      }
		      wmove(sw[4], 0, cursor);
		      update_win(4);
		      break;
		      
		  default:
		    wclear(sw[4]);
		    if(cursor != keys){
		      for(i2 = keys; i2 > cursor; i2--){
			input[i2] = input[i2 - 1];
		      }
		    }
		    input[cursor] = ci;
		    keys++;
		    cursor++;
		    for(i2 = 0; i2 < keys; i2++){
		      wprintw(sw[4], "%c", input[i2]);
		    }
		    wmove(sw[4], 0, cursor);
		    update_win(4);
		    
		    } // end switch
		  } // Return was pressed

		  wclear(sw[4]);
		  if(command_num == 0){
		    input[keys] = '\0'; // Null terminate
		  }
		  command_num = 0;

		  if(strcmp(input, "output\r") == 0){
		    wprintw(sw[4], "output: Right to left");
		    
		  } else if(strcmp(input, "outputl\r") == 0){
		    wprintw(sw[4], "outputl: output direction can't be changed for this piggy type.");
		    
		  } else if(strcmp(input, "outputr\r") == 0){
		    wprintw(sw[4], "outputr: output direction can't be changed for this piggy type.");
		    
		  } else if(strcmp(input, "dropr\r") == 0){
		    wprintw(sw[4], "dropr: a tail piggy has no right connection."); // tail has no right

		  } else if(strcmp(input, "dropl\r") == 0){
		    f_dropl = 1;
		    wprintw(sw[4], "dropl: dropping the left side connection.");
		    
		  } else if(strcmp(input, "persl\r") == 0){
		    wprintw(sw[4], "persl: Left connection is persistent.");
		    
		  } else if(strcmp(input, "persr\r") == 0){
		    wprintw(sw[4], "persr: a tail piggy can't have a right connection.");
		    
		  } else if(strcmp(input, "right\r") == 0){
		    wprintw(sw[4], "right: a tail piggy can't have a right connection.");
		    
		  } else if(strcmp(input, "left\r") == 0){
		    if(remoteport == 0 || f_droppedl == 1 || f_dropl == 1){
		      wprintw(sw[4], "Left connection: *:*:%s:%d\tNONE", localhost, localport);
		    } else {
		      wprintw(sw[4], "Left connection: %s:%d:%s:%d\t", host, remoteport, localhost, localport);
		      if(remoteport != 0){
		        wprintw(sw[4], "CONNECTED");
		      } else {
		        wprintw(sw[4], "LISTENING");
		      }
		    }
		    
		  } else if(strcmp(input, "loopr\r") == 0){
		    f_loopr = 1;
		    wprintw(sw[4], "loopr: Rightward data will go left.");
		      
		  } else if(strcmp(input, "loopl\r") == 0){
		    wprintw(sw[4], "loopl: a tail piggy won't have any use for this.");
		  
		  } else if(strcmp(input, "listenl\r") == 0){
		    if(client == 1){
		      if(FD_ISSET(ssc, &master_set)){
			close(ssc);
			FD_CLR(ssc, &master_set);
		      }
		    }

		    server = 1;
		    client = 0;

		    sa_s1.sin_port = 36762;

		    // allocate open socket
		    if((ss1 = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
		      wmove(sw[6], 0, 0);
		      update_win(6);
		      perror("socket");
		    }
		    
		    bind_listen(ss1, &sa_s1, sizeof sa_s1);
		    if(ss1 > max_sd){
		      max_sd = ss1;
		    }
		    FD_SET(ss1, &master_set);
		    
		  } else if(strcmp(input, "listenr\r") == 0){
		    wprintw(sw[4], "listenr: a tail piggy can't use listenr.");
		    
		  } else if(strcmp(input, "reset\r") == 0){
		    reset();

		  } else if((keys - 1) > 7){ // a long command (with parameters) was called

		    strncpy(&input2, &input, 1000);
		    token3 = strtok(input2, " ");
		    
		    if(strcmp(token3, "connectr") == 0){
		      wprintw(sw[4], "connectr: a tail piggy can't use connectr.");
		      
		    } else if(strcmp(token3, "connectl") == 0){
		      if(client == 1){
			if(FD_ISSET(ssc, &master_set)){
			  close(ssc);
			  FD_CLR(ssc, &master_set);
			}
		      } else {
			if(FD_ISSET(ss1, &master_set)){
			  close(ss1);
			  FD_CLR(ss1, &master_set);
			}
			if(FD_ISSET(ss2, &master_set)){
			  close(ss2);
			  FD_CLR(ss2, &master_set);
			}
		      }
		      
		      client = 1;
		      server = 0;
		      
		      token3 = strtok(NULL, " ");
		      host = token3;

		      // Look up given hostname
		      if((hpc = gethostbyname(host)) == NULL){
			fprintf(stderr, "%s: %s: no such host?\n", myname, host);
			exit(1);
		      }

		      // Put host's address and type into socket struct
		      bcopy((char *)hpc->h_addr, (char *)&sa_c.sin_addr, hpc->h_length);
		      sa_c.sin_family = hpc->h_addrtype;
		      
		      if((token3 = strtok(NULL, " "))){ // a port number was given
			if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			  sa_s1.sin_port = atoi(token3);
			  lport_given = 1;
			} else{ // not a valid port
			  wprintw(sw[6], "Error: invalid port number.");
			}
		      } else { // a port number was not given
			if(lport_given == 0){
			  sa_s1.sin_port = 36762;
			}
		      }

		      // allocate open socket
		      if((ssc = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			wmove(sw[6], 0, 0);
			update_win(6);
			perror("socket");
		      }
		      
		      connect_sock(ssc, &sa_c, sizeof sa_c);
		      f_dropl = 0;
		      f_droppedl = 0;
		      remoteport = sa_s1.sin_port;
		    
		      
		    } else if(strcmp(token3, "listenl") == 0){
		      if(client == 1){
			if(FD_ISSET(ssc, &master_set)){
			  close(ssc);
			  FD_CLR(ssc, &master_set);
			}
		      } else {
			if(FD_ISSET(ss1, &master_set)){
			  close(ss1);
			  FD_CLR(ss1, &master_set);
			}
			if(FD_ISSET(ss2, &master_set)){
			  close(ss2);
			  FD_CLR(ss2, &master_set);
			}
		      }
		      
		      client = 0;
		      server = 1;

		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			sa_s1.sin_port = atoi(token3);

			
			// allocate open socket
			if((ss1 = socket(hpc->h_addrtype, SOCK_STREAM, 0)) < 0){
			  wmove(sw[6], 0, 0);
			  update_win(6);
			  perror("socket");
			}
		      
			
			bind_listen(ss1, &sa_s1, sizeof sa_s1);
			localport = sa_s1.sin_port;
			if(ss1 > max_sd){
			  max_sd = ss1;
			}
			FD_SET(ss1, &master_set);
       
		      } else{ // not a valid port
			wprintw(sw[6], "Error: invalid port number.");
		      }
		    		      
		    } else if(strcmp(token3, "listenr") == 0){
		      wprintw(sw[4], "listenr: a tail piggy can't use listenr.");
		      
		    } else if(strcmp(token3, "read") == 0){
		      strncpy(filename2, strtok(NULL, " "), 30);
		      
		      if((file2 = fopen(filename2, "r")) != NULL){
			fgets(commands2, 300, file2);
			token2 = strtok(commands2, " ");
			if(client == 1){
			  send_sock = ssc;
			} else {
			  send_sock = ss2;
			}
			
		      } else {
			wprintw(sw[0], "read: Invalid file to read.\n");
		      }
		      
		      
		    } else if(strcmp(token3, "llport") == 0){
			  //Bind socket to service port
			  if(bind(ssc, &saddr, sizeof saddr) < 0){		
			    perror("bind");
			  }
			} else {
			  sa_s1.sin_port = atoi(token3);
			  
			  //Bind socket to service port
			  if(bind(ss1, &saddr, sizeof saddr) < 0){		
			    perror("bind");
			  }
			}
		      } else { // not a valid port
			wprintw(sw[4], "Error: invalid port number.");
		      }

		    } else if(strcmp(token3, "rlport") == 0){
		      wprintw(sw[4], "rlport: a tail piggy can't use this.");
		      
		    } else if(strcmp(token3, "lrport") == 0){
		      token3 = strtok(NULL, " ");
		      if(atoi(token3) >= 1 && atoi(token3) < 65535){ // this is a valid port
			if(client == 1){
			  sa_s1.sin_port = atoi(token3);
			  remoteport = atoi(token3);
			} else{
			  do{
			    // Accept each incoming connection
			    i3 = sizeof sa_s2;
			    if((ss2 = accept(ss1, &sa_c2, &i3)) < 0){
			      if(errno != EWOULDBLOCK){
				wmove(sw[6], 0, 0);
				update_win(6);
				perror("accept");
			      }
			    }
			    
			  }while(f_dropl == 1 || f_droppedl == 1);

			}
		      } else {
			wprintw(sw[4], "Error: invalid port number.");
		      }		      
		    }
		  
		  } else { // a short command was given, but it doesn't match anything
		    wclear(sw[4]);
		    for(i2 = 0; i2 < keys - 1; i2++){
		      wprintw(sw[4], "%c", input[i2]);
		    }
		    wprintw(sw[4], ": Unknown command");
		  }

		  keys = 0;
		  cursor = 0;
		  strncpy(&in1, &input, 1000);
		  input_change();
		  break;
		} // end switch
	      } // end while
	      	      
	    } else { // this is not the listening socket
	             // an existing connection is readable
	      close_conn = 0;

	      do{
		
		if(f_dropl == 0){
		  close_conn = 0;
		} else {
		  close_conn = 1;
		  f_droppedl = 1;
		  remoteport = 0;
		  break;
		}

		for(i2 = sizeof(buf_l2); i2 >= 0; i2--){
		  buf_l2[i2] = '\0';
		}

		// Receive data on this connection until fails
		if((rc = recv(i, &buf_l2, sizeof(buf_l2), 0)) < 0){
		  break;
		  
		} else if(rc != 0){
		  wprintw(sw[0], &buf_l2);
		  wprintw(sw[0], "\n");
		  update_win(0);
		}
		
		// Check if connection was closed by client
		if(rc == 0) {
		  wmove(sw[6], 0, 0);
		  update_win(6);
		  wprintw(sw[6], "Connection closed.");
		  close_conn = 1;
		  f_droppedl = 1;
		  remoteport = 0;
		  break;
		}
		  
		} // end if f_loopr == 1
		
	      } while(1 && close_conn == 0);

	    } // end of readable existing connection
	  } // end if FD_ISSET
	} // end for loop
	
      } while(end_server == 0);
    }while(f_persl == 1);
    
  } // end tail
  
} // end run_piggy

void update_win(int t){
  touchwin(w[t]);
  wrefresh(sw[t]);
}

void make_wins(){
  
  // Write to windows
  wmove(sw[0], 4, 0);
  if(head == 1){
    wprintw(sw[0], "Data can't arrive from the left in the head piggy.");
  } else {
    wprintw(sw[0], "Data arriving from the left.\n\n");
  }
  
  wmove(sw[1], 4, 0);
  if(tail == 0){
    waddstr(sw[1], "Data leaving the right side.\n");
  } else {
    waddstr(sw[1], "Data can't leave from the right in the tail piggy.");
  }
  
  wmove(sw[2], 4, 0);
  if(head == 1){
    waddstr(sw[2], "Data can't leave from the left side in the head piggy.");
  } else {
    waddstr(sw[2], "Data leaving the left side.\n");
  }
  
  wmove(sw[3], 4, 0);
  if(tail == 0){
    waddstr(sw[3], "Data arriving from the right.\n");
  } else {
    waddstr(sw[3], "Data can't arrive from the right in the tail piggy.");
  }
  
  wmove(sw[4], 0, 0);
  waddstr(sw[4], "Commands");
  
  wmove(sw[5], 0, 0);
  waddstr(sw[5], "Input");
  
  wmove(sw[6], 0, 0);
  waddstr(sw[6], "Errors");
  
  for(ii = 0; ii < NUMWINS; ii++){
    update_win(ii);
  }
} 

void input_change(){
  // Carries the last 10 commands
  strncpy(&in10, &in9, 1000);
  strncpy(&in9, &in8, 1000);
  strncpy(&in8, &in7, 1000);
  strncpy(&in7, &in6, 1000);
  strncpy(&in5, &in4, 1000);
  strncpy(&in4, &in3, 1000);
  strncpy(&in3, &in2, 1000);
  strncpy(&in2, &in1, 1000);

}



void reset(){
  int t;
  
  
  nocbreak();
  endwin();
  
  setlocale(LC_ALL, "");
  initscr();
  cbreak();
  noecho();
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
  clear();
  
  // create 7 windows and 7 subwindows
  for(ii = 0; ii < NUMWINS; ii++){
    w[ii] = newwin(WPOS[i][0], WPOS[ii][1], WPOS[ii][2], WPOS[ii][3]);
    sw[ii] = subwin(w[ii], WPOS[ii][0] - 2, WPOS[ii][1] - 2, WPOS[ii][2] + 1, WPOS[ii][3] + 1);
    scrollok(sw[ii], TRUE);
    wborder(w[ii], 0, 0, 0, 0, 0, 0, 0, 0);
    touchwin(w[ii]);
    wrefresh(w[ii]);
    wrefresh(sw[ii]);
  }

  
  run_piggy();
}
