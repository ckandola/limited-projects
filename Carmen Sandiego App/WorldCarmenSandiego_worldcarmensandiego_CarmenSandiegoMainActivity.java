package glacier.alpine.worldcarmensandiego;

/*
    Program:        CarmenSandiegoMainActivity
    Description:    This is the first activity of the app. It provides the user with the first two clues
                    for the first location.
    Author:         Charanjit Kandola
    Notes:          This activity should only be accessed on starting the app and after the user chooses to
                    play again, but technically pressing the back button on a tablet could bring one here.
                    Contains a static instance of CarmenSandiegoGame to be accessed by the other activities.
    Date Created:   March 5, 2018
 */


public class CarmenSandiegoMainActivity extends AppCompatActivity {
    private DatabaseManager databaseManager;
    public static CarmenSandiegoGame game;
    private TextView bubble;
    private final String TAG = "CarmenSandiegoActivity";
    private String[] clues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carmen_sandiego_main);

        databaseManager = new DatabaseManager(this);
        game = new CarmenSandiegoGame(databaseManager);

        AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.Dialog2);
        dialog.setMessage(getString(R.string.speech));
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        };
        dialog.setNeutralButton("I understand", listener);
        dialog.show();

        game.setLocation();
        Log.w(TAG, "got location " + game.getLocation().getName());

        setUp();
    }


    public void toAirplane(View view){
        Intent intent = new Intent(this, MapActivity.class);
        this.startActivity(intent);
    }

    public void onDestroy(){
        super.onDestroy();
        databaseManager.close();
    }

    public void setUp(){
        String fullIntroClues = getString(R.string.introClues);
        clues = game.getClues();
        fullIntroClues += "\n\n" + clues[0];
        fullIntroClues += "\n\n" + clues[1];
        bubble = findViewById(R.id.textView);
        bubble.setText(fullIntroClues);
    }

    public void onResume(){
        super.onResume();
        if(game.isFinished()){
            game.playAgain();
            setUp();
        }
    }
}
