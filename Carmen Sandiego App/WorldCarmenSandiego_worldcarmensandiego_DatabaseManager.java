package glacier.alpine.worldcarmensandiego;

/*
    Program:        DatabaseManager
    Description:    This is the database manager for the SQLite database.
    Author:         Charanjit Kandola
    Notes:          This only adds ClueLocations at initial startup. After that, it is only used
                    by CarmenSandiegoGame to retrieve a location by its id.
    Reference:      Extends Android's android.database.sqlite.SQLiteOpenHelper
    Date Created:   March 10, 2018
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.LocationManager;
import android.util.Log;

import java.util.ArrayList;

public class DatabaseManager extends SQLiteOpenHelper{
    private Context context;

    public DatabaseManager(Context context){
        super(context, "Location DB", null, 1);
        this.context = context;
    }

    public void onCreate(SQLiteDatabase db){
        String sqlCreate = "create table location( id integer primary key, ";
        sqlCreate += "NAME text, LATITUDE text, LONGITUDE text, CLUE1 text, CLUE2 text, CLUE3 text, CLUE4 text )";
        db.execSQL(sqlCreate);

        addLocations(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        // drop old table if it exists
        db.execSQL("drop table if exists location");
        // re-create tables
        onCreate(db);
    }

    public ArrayList<ClueLocation> selectAll(){
        String query = "select * from location";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ArrayList<ClueLocation> places = new ArrayList<ClueLocation>();
        while(cursor.moveToNext()){
            ClueLocation c = new ClueLocation(LocationManager.GPS_PROVIDER, cursor.getString(1), cursor.getString(4),
                    cursor.getString(5), cursor.getString(6), cursor.getString(7));
            c.setLatitude(Double.parseDouble(cursor.getString(2)));
            c.setLongitude(Double.parseDouble(cursor.getString(3)));
            places.add(c);
        }
        db.close();
        return places;
    }

    public ClueLocation selectById(int id) {
        String query = "select * from location where id = " + id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ClueLocation clueLocation = null;
        if (cursor.moveToFirst()) {
            clueLocation = new ClueLocation(LocationManager.GPS_PROVIDER, cursor.getString(1), cursor.getString(4),
                    cursor.getString(5), cursor.getString(6), cursor.getString(7));
            clueLocation.setLatitude(Double.parseDouble(cursor.getString(2)));
            clueLocation.setLongitude(Double.parseDouble(cursor.getString(3)));
        }
        db.close();
        return clueLocation;
    }

    public ClueLocation selectByName(String name){
        String query = "select * from location where NAME = '" + name + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ClueLocation clueLocation = null;
        if (cursor.moveToFirst()) {
            clueLocation = new ClueLocation(LocationManager.GPS_PROVIDER, cursor.getString(1), cursor.getString(4),
                    cursor.getString(5), cursor.getString(6), cursor.getString(7));
            clueLocation.setLatitude(Double.parseDouble(cursor.getString(2)));
            clueLocation.setLongitude(Double.parseDouble(cursor.getString(3)));
        }
        db.close();
        return clueLocation;
    }

    public void addLocations(SQLiteDatabase db){

        String[] places = context.getResources().getStringArray(R.array.locationsList);
        String[] latitudes = context.getResources().getStringArray(R.array.latitudes);
        String[] longitudes = context.getResources().getStringArray(R.array.longitudes);
        String[] clues = context.getResources().getStringArray(R.array.clues);

        for(int i = 0; i < places.length; i++){
            String insert = "insert into location values( " + i + ", '" + places[i] + "', '" +
                    latitudes[i] + "', '" + longitudes[i] + "', '\"" + clues[i * 4] + "\"', '\"" +
                    clues[i * 4 + 1] + "\"', '\"" + clues[i * 4 + 2] + "\"', '\"" + clues[i * 4 + 3]
                     + "\"') ";
            db.execSQL(insert);
            Log.w("DatabaseManager", "added " + places[i]);
        }
    }

}
