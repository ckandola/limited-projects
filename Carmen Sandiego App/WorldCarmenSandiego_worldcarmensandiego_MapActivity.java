package glacier.alpine.worldcarmensandiego;

/*
    Program:        MapActivity
    Description:    This is the second activity that is played repeatedly. It uses a Google MapFragment to help the
                    user determine where to go to next.
    Author:         Charanjit Kandola
    Notes:          This uses a Constraint Layout, but it contains a TableRow Layout for the Buttons.
                    The text on the buttons are from randomly chosen locations and the one correct location from the
                    Game.
                    Clicking on the markers only moves the map; to travel, a user has to press the Buttons.
                    Animation of plane and a sound effect play when a Button is pressed.
    Reference:      Uses the Google Map API v2
                    Plane image from    http://images.clipartpanda.com/cartoon-airplane-clipart-niXn8XpAT.jpeg
                    Sound effect from   https://www.freesoundeffects.com/free-sounds/airplane-10004/
    Date Created:   March 9, 2018
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Random;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private Intent intent;
    private DatabaseManager dbm;
    private CarmenSandiegoGame carmenSandiegoGame;

    private Button button1;
    private Button button2;
    private Button button3;
    private SupportMapFragment supportMapFragment;
    private GoogleMap map;
    private ImageView plane;

    private String chosenText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        dbm = new DatabaseManager(this);

        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
        supportMapFragment.getMapAsync((OnMapReadyCallback) this);

        carmenSandiegoGame = CarmenSandiegoMainActivity.game;

        AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.Dialog2);
        dialog.setMessage(getString(R.string.help_dialog_map));
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        };
        dialog.setNeutralButton("I understand", listener);
        dialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        UiSettings settings = map.getUiSettings();
        settings.setTiltGesturesEnabled(false);
        settings.setRotateGesturesEnabled(false);

        updateView();

        // add marker at HQ (CF Building)
        map.addMarker(new MarkerOptions().position(new LatLng(48.732768, -122.485227)).title("Crime Fighting HQ"));
        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(48.732768, -122.485227)));
    }

    public void updateView() {
        Button[] buttons = {button1, button2, button3};
        ClueLocation[] extras = carmenSandiegoGame.getLocations();

        map.clear();
        // add 3 markers
        for (int i = 0; i < 2; i++) {
            ClueLocation c = extras[i];
            map.addMarker(new MarkerOptions().position(new LatLng(c.getLatitude(), c.getLongitude())).title(c.getName()));
        }
        if (carmenSandiegoGame.getLastLocation() != null) {
            ClueLocation l = carmenSandiegoGame.getLastLocation();
            LatLng myPlace = new LatLng(l.getLatitude(), l.getLongitude());
            map.addMarker(new MarkerOptions().position(myPlace).title(l.getName()));
            map.moveCamera(CameraUpdateFactory.newLatLng(myPlace));
        }

        ClueLocation current = carmenSandiegoGame.getLocation();
        map.addMarker(new MarkerOptions().position(new LatLng(current.getLatitude(), current.getLongitude())).title(current.getName()));

        String[] names = {carmenSandiegoGame.getLocation().getName(), extras[0].getName(), extras[1].getName()};
        Random random = new Random();
        int[] used = {3, 3, 3};

        for (int i = 0; i < 3; i++) {
            int id = random.nextInt(3);
            while (id == used[0] || id == used[1]) {
                id = random.nextInt(3);
            }
            buttons[i].setText(names[id]);
            used[i] = id;
        }
    }

    public void onRestart() {
        super.onRestart();
        if (carmenSandiegoGame.getTries() == 0 || carmenSandiegoGame.getNumCorrect() == 4) {
            this.finish();
        }
        updateView();
    }

    public void animate(View view) {

        final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.plane);

        chosenText = (String) ((Button) view).getText();
        plane.setVisibility(View.VISIBLE);
        TranslateAnimation animation = new TranslateAnimation(0, 1510, 0, -200);
        animation.setDuration(2500);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mediaPlayer.start();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                plane.setVisibility(View.GONE);
                carmenSandiegoGame.makeAttempt();
                Log.w("MapActivity", "tries is now " + carmenSandiegoGame.getTries());
                Log.w("MapActivity", "button name: " + chosenText);
                carmenSandiegoGame.setLastLocation(dbm.selectByName(chosenText));
                Log.w("MapActivity", "set lastLocation to " + carmenSandiegoGame.getLastLocation().getName());

                Intent newIntent = new Intent(MapActivity.this, ResultActivity.class);
                startActivity(newIntent);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        plane.startAnimation(animation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        Log.w("MapActivity", "Inflating menu");
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        dialog.setNeutralButton("Got it", listener);
        if(id == R.id.help){
            title.setText(R.string.help);
            dialog.setMessage(R.string.help_dialog_map);
        } else if(id == R.id.clues){
            title.setText(R.string.clues);
            String[] clues = carmenSandiegoGame.getTwoClues();
            String giveClues = "Clue 1: " + clues[0];
            giveClues += "\n\nClue 2: " + clues[1];
            dialog.setMessage(giveClues);
        } else if(id == R.id.tries){
            title.setText(R.string.tries);
            String tries = "You have " + carmenSandiegoGame.getTries() + " tries left.";
            dialog.setMessage(tries);
        }
        AlertDialog alert = dialog.show();
        TextView textView = (TextView) alert.findViewById(android.R.id.message);
        textView.setTypeface(null, Typeface.BOLD_ITALIC);
        return true;
    }
}
